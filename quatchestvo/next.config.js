/** @type {import('next').NextConfig} */

module.exports = {
  future: { webpack5: true },
  reactStrictMode: true,
  i18n: {
    locales: ['en', 'ru', 'ua'],
    defaultLocale: 'ru',
  },

  webpack(config) {
    config.module.rules.push({
      // SVG Loader
      test: /\.svg$/,
      use: [{ loader: '@svgr/webpack' }],
    })
    return config
  },
}