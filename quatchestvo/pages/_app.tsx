import React from 'react';
import { useRouter } from 'next/router';
import { IntlProvider } from 'react-intl';

// Translations
import messages_en from '../lang/en.json';
import messages_ru from '../lang/ru.json';
import messages_ua from '../lang/ua.json';

const App = ({ Component, pageProps }) => {
  const route = useRouter();

  // Locale Messages
  const messages = {
    'en': messages_en,
    'ru': messages_ru,
    'ua': messages_ua,
  };

  return (
    <IntlProvider locale={route?.locale} messages={messages[route?.locale]}>
      <Component {...pageProps} />
    </IntlProvider>
  )
};

export default App
