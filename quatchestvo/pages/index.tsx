import { GetStaticPropsContext } from 'next'
import Head from 'next/head';
import reset from 'react-style-reset';
import { Global } from '@emotion/react';
import { Toaster } from 'react-hot-toast';

// PageView
import HomePage from '../src/pageView/HomePage';

// Global style
import style from '../src/common/style/global.style';

const Home = () => {

  return (
    <div>
      <Global styles={[reset, style()]} />
      <Head>
        <title>QA курсы тестирования ПО</title>
        <meta name="description" content="Generated by create next app" />
        <link rel="icon" href="/favicon.ico" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css" />
      </Head>

      <HomePage />
      <div id="modal" />
      <Toaster />
    </div>
  )
}

export default Home;
