import { useState, useEffect } from 'react';

import { isDesktop, isMobile } from 'react-device-detect';

const useDeviceDetect = () => {
  // State
  const [device, setDevice] = useState<string | null>(null);

  useEffect(() => {
    switch (true) {
      case isMobile:
        return setDevice('mobile');

      case isDesktop:
        return setDevice('desktop');

      default:
        return setDevice(null);
    }

  }, [])

  return device;
}

export default useDeviceDetect;