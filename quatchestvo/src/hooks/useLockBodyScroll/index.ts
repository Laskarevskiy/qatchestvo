import { useLayoutEffect } from 'react';

const useLockBodyScroll = () => {
  useLayoutEffect(() => {
    // Prevent scrolling on mount
    if (document?.body) {
      document.body.style.overflow = 'hidden';
      document.body.style.width = '100%';
      document.body.style.height = '100%';
    }
    // Re-enable scrolling when component unmounts
    return () => {
      if (document?.body) {
        document.body.style.overflow = 'visible';
        document.body.style.position = 'relative';
      }
    };
  }, []); // Empty array ensures effect is only run on mount and unmount
}

export default useLockBodyScroll;