/** @jsxRuntime classic */
/** @jsx jsx */
import { css } from '@emotion/react';

// Colors
import {
  primaryWhite,
  primaryBlue,
  secondaryGrey,
} from '@/common/color';

// Config
import {
  mediaMobileTablet,
  mediaDesktop,
} from '@/config';

const style = () => css`
  /* Common style */
  .main-container-fluid {
    background: rgba(0, 122, 255, 0.1);
  }

  .switch-profession-container-fluid {
    background: ${secondaryGrey};
  }

  .free-consultation-container-fluid {
    background: ${primaryWhite};
  }

  .course-path-container-fluid {
    background: ${primaryWhite};
  }

  .detail-course-program-container-fluid {
    background: ${primaryWhite};

    &.mobile {
      background: #E5F2FF;
    }
  }

  .bonus-container-fluid {
    background: ${primaryWhite};
  }

  .learn-technology-container-fluid {
    background: ${primaryWhite};
  }

  .after-course-container-fluid {
    background: ${primaryWhite};
  }

  .course-price-container-fluid {
    background: ${primaryBlue};
  }

  .course-entry-requirements-container-fluid {
    background: ${secondaryGrey};
  }

  .course-benefits-container-fluid {
    background: ${primaryWhite};
  }

  .course-teachers-container-fluid {
    background: ${primaryWhite};
  }

  .asked-question-container-fluid {
    background: ${primaryWhite};
  }

  .feedbacks-students-container-fluid {
    background: #E5F2FF;
  }

  .contacts-container-fluid {
    background: ${primaryWhite};
  }

  ${mediaMobileTablet} {
    .container-mobile {
      padding: 40px 16px;
    }
  }

  /* Style desktop => */
  ${mediaDesktop} {
    .container-desktop {
      padding: 64px 16px;
    }
  }
`;

export default style;