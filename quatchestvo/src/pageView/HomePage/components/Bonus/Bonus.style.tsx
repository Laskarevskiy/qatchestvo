/** @jsxRuntime classic */
/** @jsx jsx */
import { css } from '@emotion/react';

// Colors
import { secondaryGrey } from '@/common/color';

// Config
import {
  mediaMobileTablet,
  mediaDesktop,
} from '@/config';

const style = () => css`
  /* Common style */
  background: ${secondaryGrey};
  border-radius: 16px;

  .bonus-course {
    display: flex;
    align-items: center;

    .SubHeading {
      margin-right: 8px;
    }
  }

  .bonus-list {
    list-style: disc;
  }

  /* Style mobile => tablet */
  ${mediaMobileTablet} {
    padding: 32px 16px;

    .bonus-course {
      margin-bottom: 24px;
    }

    .bonus-list {
      margin-left: 16px;
    }
  }

  /* Style desktop => */
  ${mediaDesktop} {
    display: flex;
    flex-direction: column;
    align-items: flex-start;
    padding: 48px 80px;

    .bonus-course {
      margin-bottom: 16px;
    }

    .bonus-list {
      display: flex;
      flex-direction: column;
      align-items: flex-start;
      margin-left: 20px;
    }
  }
`;

export default style;