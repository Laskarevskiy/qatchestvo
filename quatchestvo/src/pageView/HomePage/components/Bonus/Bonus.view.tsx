/** @jsxRuntime classic */
/** @jsx jsx */
import { jsx } from '@emotion/react';
import ScrollAnimation from 'react-animate-on-scroll';

// Components
import SubHeading from '@/components/Typography/SubHeading';
import Paragraph from '@/components/Typography/Paragraph';

// Icons
import Fire from '@/static/images/Fire.svg';

// Style
import style from './Bonus.style';

const Bonus = () => {

  return (
    <ScrollAnimation animateIn='fadeIn'>
      <section css={style()}>
        <div className="bonus-course">
          <SubHeading
            text="Бонусы курса"
            type="h4"
          />
          <Fire />
        </div>
        <ul className="bonus-list">
          <li className="bonus-list-item">
            <Paragraph
              text="Английский Speaking"
              type="small"
            />
          </li>
          <li className="bonus-list-item">
            <Paragraph
              text="Тренинг по прохождению собеседования и составлению резюме"
              type="small"
            />
          </li>
        </ul>
      </section>
    </ScrollAnimation>
  );
};

export default Bonus;
