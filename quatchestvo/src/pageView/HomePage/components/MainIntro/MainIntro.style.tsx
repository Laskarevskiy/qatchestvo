/** @jsxRuntime classic */
/** @jsx jsx */
import { css } from '@emotion/react';

// Config
import {
  mediaMobileTablet,
  mediaMobileMax,
  mediaDesktop,
} from '@/config';

const style = () => css`
  /* Common style */
  @keyframes rotate {
    to {
      transform: rotate(360deg);
    }
  }

  position: relative;
  display: flex;
  overflow: hidden;

  .main-intro-left-container {
    .Heading {
      font-weight: 800;
      margin-bottom: 16px;
    }

    .Paragraph {
      margin-bottom: 32px;
    }

    .course-action-buttons {
      display: flex;
      flex-direction: column;
      align-items: flex-start;

      .Button:not(:last-of-type) {
        margin-bottom: 8px;
      }
    }
  }

  .main-intro-course-list__item {
    display: flex;
    align-items: center;

    &:not(:last-of-type) {
      margin-bottom: 24px;
    }

    svg {
      min-width: 32px;
      margin-right: 16px;
    }

    .main-intro-course-list__item--content {
      .Paragraph {
        line-height: 28px;
        font-weight: 700;
        margin-bottom: 4px;
      }

      .Caption {
        text-align: start;
        word-break: break-word;
      }
    }
  }

  /* Style mobile => tablet */
  ${mediaMobileTablet} {
    padding: 40px 0;

    .main-intro-left-container {
      margin-right: 10px;

      .Heading {
        font-size: 32px;
        line-height: 38px;
        word-break: break-word;
      }

      .Paragraph {
        font-size: 16px;
        line-height: 26px;
      }

      .main-intro-buttons-container {
        margin-bottom: 48px;

        .course-action-buttons {
          .Button {
            width: 100%;
          }
        }
      }
    }

    .main-intro-right-container {
      margin-left: 10px;

      .main-intro-course-list {
        margin-bottom: 20px;
      }
    }

    ${mediaMobileMax} {
      flex-direction: column;

      .main-intro-left-container,
      .main-intro-right-container {
        margin: 0;
      }
    }
  }

  /* Style desktop => */
  ${mediaDesktop} {
    justify-content: space-between;
    padding: 196px 0 230px;

    .LogoDeco_svg__icon {
      position: absolute;
      width: 100%;
      height: 100%;
      animation: rotate 50s linear infinite;
      top: 0;
      z-index: -1000;
    }

    .main-intro-left-container {
      max-width: 540px;
      width: 100%;
      margin-right: 20px;

      .Heading {
        font-size: 48px;
        line-height: 64px;
      }

      .course-action-buttons {
        margin-bottom: 16px;
      }
    }

    .main-intro-right-container {
      margin-left: 20px;
    }
  }
`;

export default style;