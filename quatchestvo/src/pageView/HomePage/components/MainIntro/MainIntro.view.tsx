/** @jsxRuntime classic */
/** @jsx jsx */
import { jsx } from '@emotion/react';

// Hooks
import useDeviceDetect from '@/hooks/useDeviceDetect/index';

// Config
import { DEVICE_DESKTOP } from '@/config';

// Components
import Heading from '@/components/Typography/Heading';
import Paragraph from '@/components/Typography/Paragraph';
import Caption from '@/components/Typography/Caption';
import CourseActionButtons from '@/components/CourseActionButtons';
import StudentsWork from '@/components/StudentsWork';

// Icons
import IconCalendar from '@/static/images/IconCalendar.svg';
import IconClock from '@/static/images/IconClock.svg';
import Computer from '@/static/images/Computer.svg';
import Ticket from '@/static/images/Ticket.svg';
import LogoDeco from '@/static/images/LogoDeco.svg';

// Style
import style from "./MainIntro.style";

const  MainIntro = () => {
  // Constants
  const device = useDeviceDetect();
  const isDesktopDevice = device === DEVICE_DESKTOP;


  const mainIntroData = [
    {
      icon: <IconCalendar />,
      text: 'Старт группы',
      description: 'По набору',
    },
    {
      icon: <IconClock />,
      text: 'Длительность',
      description: '2-4 месяца (зависит от усвоения материала)',
    },
    {
      icon: <Computer />,
      text: 'Формат',
      description: 'онлайн',
    },
    {
      icon: <Ticket />,
      text: 'Цена',
      description: '$600 за курс или Оплата частями = 2х300$',
    },
  ];

  return (
    <section css={style()}>
      {isDesktopDevice && (
        <LogoDeco />
      )}
      <div className="main-intro-left-container">
        <Heading text="Курсы QA тестирования" />
        <Paragraph text="Наша цель - твой успех в новой професии" />

        <div className="main-intro-buttons-container">
          <CourseActionButtons analytics="main_intro" />
        </div>
      </div>

      <div className="main-intro-right-container">
        <ul className="main-intro-course-list">
          {mainIntroData.map(item => (
            <li key={item.text} className="main-intro-course-list__item">
              {item.icon}
              <div className="main-intro-course-list__item--content">
                <Paragraph text={item.text} weight="700"/>
                <Caption text={item.description} type="medium" />
              </div>
            </li>
          ))}
        </ul>
      </div>
    </section>
  );
};

export default  MainIntro;