/** @jsxRuntime classic */
/** @jsx jsx */
import { css } from '@emotion/react';

// Colors
import { primaryBlue } from '@/common/color';

// Config
import {
  mediaMobileTablet,
  mediaDesktop,
} from '@/config';

const style = () => css`
  /* Common style */
  &.detail-course-program-list__item-container {
    display: flex;
    background: #E5F2FF;

    .detail-course-program-list__item-count {
      font-size: 28px;
      line-height: 40px;
      font-weight: 700;
      color: ${primaryBlue};
    }

    .detail-course-program-content-container {
      display: flex;
      justify-content: space-between;
      width: 100%;
      cursor: pointer;
    }

    .detail-course-program-list__item-title {
      font-size: 14px;
      font-weight: 700;
      line-height: 18px;
      margin-bottom: 4px;
      text-transform: uppercase;
    }

    .Caption.medium {
      text-align: start;
    }
  }

  /* Style mobile => tablet */
  ${mediaMobileTablet} {
    &.detail-course-program-list__item-container {
      padding: 24px 0px 32px;

      .detail-course-program-list__item-count {
        margin-right: 16px;
      }
    }
  }

  /* Style desktop => */
  ${mediaDesktop} {
    &.detail-course-program-list__item-container {
      padding: 24px 38px 24px 80px;

      .detail-course-program-list__item-count {
        margin-right: 80px;
      }

      .IconCircleArrowFilledWhite_svg__icon {
        min-width: 32px;
        margin-left: 24px;
        transform: rotate(180deg);
      }

      .detail-course-program-content-container  {
        &.open-description {
          .IconCircleArrowFilledWhite_svg__icon {
            transform: rotate(0deg);
          }
        }
      }
    }
  }
`;

export default style;