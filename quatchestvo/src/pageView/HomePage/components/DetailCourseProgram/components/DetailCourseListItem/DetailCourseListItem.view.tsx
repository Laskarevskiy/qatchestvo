/** @jsxRuntime classic */
/** @jsx jsx */
import { useState } from 'react';
import { jsx } from '@emotion/react';
import SmoothCollapse from 'react-smooth-collapse';
import { isDesktop } from 'react-device-detect';

// Components
import Caption from '@/components/Typography/Caption';

// Icons
import IconCircleArrowFilledWhite from '@/static/images/IconCircleArrowFilledWhite.svg';

// Style
import style from "./DetailCourseListItem.style";

interface IAskedQuestionListItem {
  title: string,
  text: string,
  count: number,
  isMobile?: boolean,
}

const  DetailCourseListItem = (props: IAskedQuestionListItem) => {
  const {
    count,
    title,
    text,
    isMobile,
  } = props;

  // State
  const [isOpenDescription, setOpenDescription] = useState<boolean>(false);

  // Constants
  const isOpenDescriptionClassName = isOpenDescription ? 'open-description' : '';

  // Handlers
  const toogleDescriptionClickHandler = () => {
    if (isMobile) return;

    setOpenDescription(prevState => !prevState)
  };

  return (
    <li css={style()} className="detail-course-program-list__item-container">
      <span className="detail-course-program-list__item-count">
        {count}
      </span>
      <div className={`detail-course-program-content-container ${isOpenDescriptionClassName}`} onClick={toogleDescriptionClickHandler}>
        <div>
          <h5 className="detail-course-program-list__item-title">
            {title}
          </h5>

          {isMobile ? (
            <Caption text={text} type="medium" />
          ) : (
            <SmoothCollapse
              expanded={isOpenDescription}
              heightTransition=".35s ease"
            >
              <Caption text={text} type="medium" />
            </SmoothCollapse>
          )}
        </div>

        {isDesktop && (
          <IconCircleArrowFilledWhite />
        )}
      </div>
    </li>
  );
};

export default  DetailCourseListItem;