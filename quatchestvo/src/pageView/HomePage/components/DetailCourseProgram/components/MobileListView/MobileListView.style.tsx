/** @jsxRuntime classic */
/** @jsx jsx */
import { css } from '@emotion/react';

// Colors
import {  } from '@/common/color';

const style = () => css`
  .Button.white-button {
    width: 100%;
  }
`;

export default style;