/** @jsxRuntime classic */
/** @jsx jsx */
import React, { useState } from 'react';
import { jsx } from '@emotion/react';
import SmoothCollapse from 'react-smooth-collapse';

// Components
import Button from '@/components/Button';
import DetailCourseListItem from '../DetailCourseListItem';

// Style
import style from "./MobileListView.style";

// Mock Data
import detailCourseList from '../detail_course_list.json';

const  MobileListView = () => {
  // State
  const [isShowFullList, setShowFullList] = useState<boolean>(false);

  // Constants
  const visibleElementsFromList = 2;

  // Handlers
  const toogleShowFullListHandler = () => setShowFullList(prevState => !prevState);

  return (
    <div css={style()}>
      <ul>
        {detailCourseList.map((item, index) => (
          (index + 1) > visibleElementsFromList ? (
            <SmoothCollapse
              expanded={isShowFullList}
              heightTransition=".35s ease"
            >
              <DetailCourseListItem
                key={item.title}
                title={item.title}
                text={item.text}
                count={index + 1}
                isMobile
              />
            </SmoothCollapse>
          ) : (
            <DetailCourseListItem
              key={item.title}
              title={item.title}
              text={item.text}
              count={index + 1}
              isMobile
            />
          )
        ))}
      </ul>

      <Button
        type="white-button"
        textSize="text-small"
        onClick={toogleShowFullListHandler}
        text={isShowFullList ? 'Скрыть' : 'Смотреть больше'}
      />
    </div>
  );
};

export default  MobileListView;