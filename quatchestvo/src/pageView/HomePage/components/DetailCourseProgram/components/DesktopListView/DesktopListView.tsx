/** @jsxRuntime classic */
/** @jsx jsx */
import { jsx } from '@emotion/react';

// Components
import DetailCourseListItem from '../DetailCourseListItem';

// Mock Data
import detailCourseList from '../detail_course_list.json';

const  DesktopListView = () => {

  return (
    <ul>
      {detailCourseList.map((item, index) => (
        <DetailCourseListItem
          key={item.title}
          title={item.title}
          text={item.text}
          count={index + 1}
        />
      ))}
    </ul>
  );
};

export default  DesktopListView;