/** @jsxRuntime classic */
/** @jsx jsx */
import { jsx } from '@emotion/react';
import ScrollAnimation from 'react-animate-on-scroll';
import { isDesktop } from 'react-device-detect';

// Components
import SubHeading from '@/components/Typography/SubHeading';
import DesktopListView from './components/DesktopListView';
import MobileListView from './components/MobileListView';

// Style
import style from "./DetailCourseProgram.style";

// Mock Data
// import askedQuestionList from './asked_question_list.json';

const  DetailCourseProgram = () => {

  return (
    // <ScrollAnimation animateIn='fadeIn'>
      <div css={style()}>
        <SubHeading text="Детальная программа курса" type="h2" />
        {isDesktop ? (
          <DesktopListView />
        ) : (
          <MobileListView />
        )}

      </div>
    /* </ScrollAnimation> */
  );
};

export default  DetailCourseProgram;