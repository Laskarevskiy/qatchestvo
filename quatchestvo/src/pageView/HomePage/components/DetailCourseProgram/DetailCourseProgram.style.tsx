/** @jsxRuntime classic */
/** @jsx jsx */
import { css } from '@emotion/react';

// Colors
import {  } from '@/common/color';

// Config
import {
  mediaMobileTablet,
  mediaDesktop,
} from '@/config';

const style = () => css`
  /* Common style */

  /* Style mobile => tablet */
  ${mediaMobileTablet} {
    .SubHeading.h2 {
      margin-bottom: 24px;
    }
  }

  /* Style desktop => */
  ${mediaDesktop} {
    .SubHeading.h2 {
      margin-bottom: 48px;
    }
  }
`;

export default style;