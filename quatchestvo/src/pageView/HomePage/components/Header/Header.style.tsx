/** @jsxRuntime classic */
/** @jsx jsx */
import { css } from '@emotion/react';

// Config
import { mediaMobileTablet, mediaDesktop } from '@/config';

const style = () => css`
  /* Common style */
  padding-top: 16px;

  .left-container {
    display: flex;

    .main-logo {
      display: inline-block;
      background-position: center;
      background-repeat: no-repeat;
      cursor: pointer;
    }
  }

  /* Style mobile => tablet */
  ${mediaMobileTablet} {
    .left-container {
      align-items: center;
      justify-content: space-between;

      .main-logo {
        margin-right: 16px;
        background-image: url('/static/images/Logo.svg');
        min-width: 113px;
        width: 113px;
        min-height: 22px;
        height: 22px;
        margin-right: 12px;
      }
    }

    .course-action-buttons {
      display: none;
    }
  }

  /* Style desktop => */
  ${mediaDesktop} {
    display: flex;
    align-items: center;
    justify-content: space-between;

    .left-container {
      justify-content: flex-start;

      .main-logo {
        background-image: url('/static/images/Logo.svg');
        min-width: 146px;
        width: 146px;
        min-height: 28px;
        height: 28px;
        margin-right: 32px;
      }
    }

    .course-action-buttons {
      display: block;

      .Button.primary {
        margin-left: 14px;
      }
    }
  }
`;

export default style;