/** @jsxRuntime classic */
/** @jsx jsx */
import { jsx } from '@emotion/react';
import Link from 'next/link';

// Components
import Dropdown from '@/components/Dropdown';
import CourseActionButtons from '@/components/CourseActionButtons';

// Style
import style from "./Header.style";

const Header = () => {

  return (
    <header css={style()}>
      <div className="left-container">
        <Link href="/" passHref>
          <span className="main-logo" />
        </Link>
        {/* <Dropdown /> */}
      </div>

      <CourseActionButtons heightButton="40" analytics="header_buttons_desktop" />
    </header>
  );
};

export default Header;