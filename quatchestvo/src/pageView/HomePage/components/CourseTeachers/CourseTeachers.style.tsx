/** @jsxRuntime classic */
/** @jsx jsx */
import { css } from '@emotion/react';

// Colors
import {
  primaryWhite,
  primaryBlue,
} from '@/common/color';

// Config
import {
  mediaMobileTablet,
  mediaDesktop,
} from '@/config';

const style = () => css`
  /* Common style */

  .SubHeading.h2 {
    margin-bottom: 24px;
  }

  .course-teachers-list {
    display: flex;
  }

  /* Style mobile => tablet */
  ${mediaMobileTablet} {
    .course-teachers-description {
      margin-bottom: 40px;
    }
  }

  /* Style desktop => */
  ${mediaDesktop} {
    .course-teachers-list {
      display: grid;
      grid-template-columns: 1fr 1fr 1fr 1fr;
      grid-row-gap: 64px;
    }

    .course-teachers-description {
      margin-bottom: 48px;
    }
  }

  .rec-slider-container {
    margin: 0;
  }


  /* Slider */
  .rec-carousel {
    position: relative;
  }

  .rec-arrow-right {
    position: absolute;
    right: -30px;
  }

  .rec-arrow-left {
    position: absolute;
    left: -30px;
  }

  .rec-arrow {
    min-width: 48px;
    width: 48px;
    min-height: 48px;
    height: 48px;
    background: ${primaryWhite};
    color: #929396;
    box-shadow: 0px 10px 20px rgba(41, 41, 42, 0.07);
    z-index: 100;

    &:hover,
    &:focus {
      background: ${primaryWhite} !important;
      color: ${primaryBlue} !important;
    }
  }
`;

export default style;