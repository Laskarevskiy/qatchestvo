/** @jsxRuntime classic */
/** @jsx jsx */
import { jsx } from '@emotion/react';
import ScrollAnimation from 'react-animate-on-scroll';
import Carousel from 'react-elastic-carousel';

// Hooks
import useDeviceDetect from '@/hooks/useDeviceDetect/index';

// Config
import { DEVICE_DESKTOP } from '@/config';

// Components
import SubHeading from '@/components/Typography/SubHeading';
import Paragraph from '@/components/Typography/Paragraph';
import CardTeacher from '@/components/CardTeacher';

// Style
import style from "./CourseTeachers.style";

// Mock Data
import courseTeachersData from './course_teachers_data.json';

const  CourseTeachers = () => {
  // Constants
  const device = useDeviceDetect();
  const isDesktopDevice = device === DEVICE_DESKTOP;

  const breakPoints = [
    { width: 1, itemsToShow: 1 },
    { width: 500, itemsToShow: 2 },
    { width: 768, itemsToShow: 3 },
    { width: 1100, itemsToShow: 4 }
  ];

  return (
    <ScrollAnimation animateIn='fadeIn'>
      <section css={style()}>
        <SubHeading text="Преподаватели и менторы" type="h2" />
        <Paragraph
          customClassName="course-teachers-description"
          text="Во время обучения вас будут сопровождать опытные преподаватели, специалисты своих направлений. А также у каждой группы есть свой ментор, который обеспечивает поддержку 24/7."
          type="medium"
        />

        <Carousel
          isRTL={false}
          showArrows={isDesktopDevice}
          pagination={false}
          breakPoints={breakPoints}
          itemPosition="CENTER"
        >
          {courseTeachersData.map(item => (
            <CardTeacher
              key={item.name}
              photo={item.photo}
              name={item.name}
              position={item.position}
              description={item.description}
              linkedinLink={item.linkedinLink}
              linkedinName={item.linkedinName}
            />
          ))}
        </Carousel>
      </section>
    </ScrollAnimation>
  );
};

export default  CourseTeachers;