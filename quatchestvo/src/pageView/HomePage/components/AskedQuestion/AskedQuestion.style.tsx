/** @jsxRuntime classic */
/** @jsx jsx */
import { css } from '@emotion/react';

// Colors
import { secondaryGrey, primaryWhite } from '@/common/color';

// Config
import {
  mediaMobileTablet,
  mediaDesktop,
} from '@/config';

const style = () => css`
  /* Common style */
  display: flex;

  .asked-question-call-back-form {
    border: 2px solid ${secondaryGrey};
    border-radius: 8px;
    background: ${primaryWhite};
    filter: drop-shadow(-2px 4px 45px rgba(0, 0, 0, 0.13));
  }


  /* Style mobile => tablet */
  ${mediaMobileTablet} {
    flex-direction: column-reverse;

    .asked-question-call-back-form {
      padding: 32px 16px;
    }
  }

  /* Style desktop => */
  ${mediaDesktop} {
    justify-content: space-between;

    .asked-question-call-back-form {
      max-width: 540px;
      width: 100%;
      padding: 64px;
    }
  }
`;

export default style;