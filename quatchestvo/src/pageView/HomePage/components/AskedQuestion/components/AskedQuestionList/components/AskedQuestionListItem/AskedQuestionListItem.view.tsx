/** @jsxRuntime classic */
/** @jsx jsx */
import { useState } from 'react';
import { jsx } from '@emotion/react';
import SmoothCollapse from 'react-smooth-collapse';

// Components
import SubHeading from '@/components/Typography/SubHeading';
import Paragraph from '@/components/Typography/Paragraph';

// Icons
import IconArrow from '@/static/images/IconArrow.svg';

// Style
import style from "./AskedQuestionListItem.style";

interface IAskedQuestionListItem {
  title: string,
  text: string,
}

const  AskedQuestionListItem = (props: IAskedQuestionListItem) => {
  const { title, text } = props;

  // State
  const [isOpenDescription, setOpenDescription] = useState<boolean>(false);

  // Constants
  const isOpenDescriptionClassName = isOpenDescription ? 'open-description' : '';

  // Handlers
  const toogleDescriptionClickHandler = () => setOpenDescription(prevState => !prevState);

  return (
    <li css={style} className="asked-question-list__item">
      <div className={`questions-title-wrap ${isOpenDescriptionClassName}`} onClick={toogleDescriptionClickHandler}>
        <SubHeading text={title} type="h6" />

        <IconArrow />
      </div>

      <SmoothCollapse
        expanded={isOpenDescription}
        heightTransition=".35s ease"
      >
        <Paragraph text={text} type="medium" />
      </SmoothCollapse>
    </li>
  );
};

export default  AskedQuestionListItem;