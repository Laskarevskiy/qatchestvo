/** @jsxRuntime classic */
/** @jsx jsx */
import { jsx } from '@emotion/react';
import ScrollAnimation from 'react-animate-on-scroll';

// Components
import SubHeading from '@/components/Typography/SubHeading';
import AskedQuestionListItem from './components/AskedQuestionListItem';

// Style
import style from "./AskedQuestionList.style";

// Mock Data
import askedQuestionList from './asked_question_list.json';

const  AskedQuestionList = () => {

  return (
    <ScrollAnimation animateIn='fadeIn'>
      <div css={style()}>
        <SubHeading text="Часто задаваемые воросы" type="h2" />

        <ul className="asked-question-list">
          {askedQuestionList.map(item => (
            <AskedQuestionListItem
              key={item.title}
              title={item.title}
              text={item.text}
            />
          ))}
        </ul>
      </div>
    </ScrollAnimation>
  );
};

export default  AskedQuestionList;