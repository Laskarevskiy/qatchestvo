/** @jsxRuntime classic */
/** @jsx jsx */
import { css } from '@emotion/react';

// Colors
import { neutralDarkGrey } from '@/common/color';

// Config
import {
  mediaMobileTablet,
  mediaDesktop,
} from '@/config';

const style = () => css`
  /* Common style */
  &.asked-question-list__item {
    cursor: pointer;

    .questions-title-wrap  {
      display: flex;
      align-items: flex-start;
      justify-content: space-between;

      .IconArrow_svg__arrow {
        min-width: 25px;
        margin: 4px 0 0 16px;
      }

      &.open-description {
        .IconArrow_svg__arrow {
          transition: all 0.2s ease 0s;
          transform: rotate(180deg);
        }
      }
    }
  }

  .Paragraph {
    margin-top: 8px;
    color: ${neutralDarkGrey};
  }

  /* Style mobile => tablet */
  ${mediaMobileTablet} {
    &.asked-question-list__item {
      margin-bottom: 24px;
    }
  }

  /* Style desktop => */
  ${mediaDesktop} {
    &.asked-question-list__item {
      margin-bottom: 32px;
    }
  }
`;

export default style;