/** @jsxRuntime classic */
/** @jsx jsx */
import { css } from '@emotion/react';

// Colors
import {  } from '@/common/color';

// Config
import {
  mediaMobileTablet,
  mediaDesktop,
} from '@/config';

const style = () => css`
  /* Common style */
  .asked-question-list {
    width: 100%;
  }

  /* Style mobile => tablet */
  ${mediaMobileTablet} {
    .SubHeading.h2 {
      margin-bottom: 24px;
    }
  }

  /* Style desktop => */
  ${mediaDesktop} {
    margin-left: 78px;

    .SubHeading.h2 {
      margin-bottom: 32px;
    }
  }
`;

export default style;