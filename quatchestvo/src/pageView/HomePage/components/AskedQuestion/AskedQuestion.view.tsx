/** @jsxRuntime classic */
/** @jsx jsx */
import { jsx } from '@emotion/react';
import ScrollAnimation from 'react-animate-on-scroll';

// Components
import CallBackForm from '@/components/CallBackForm';
import AskedQuestionList from './components/AskedQuestionList';

// Style
import style from "./AskedQuestion.style";

const  AskedQuestion = () => {

  return (
    <ScrollAnimation animateIn='fadeIn'>
      <section css={style()}>
        <div className="asked-question-call-back-form">
          <CallBackForm analytics="asked_question" />
        </div>
        <AskedQuestionList />
      </section>
    </ScrollAnimation>
  );
};

export default  AskedQuestion;