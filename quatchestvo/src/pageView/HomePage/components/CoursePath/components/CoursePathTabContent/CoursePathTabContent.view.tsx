/** @jsxRuntime classic */
/** @jsx jsx */
import { jsx } from '@emotion/react';

// Hooks
import useDeviceDetect from '@/hooks/useDeviceDetect/index';

// Config
import { DEVICE_DESKTOP } from '@/config';

// Components
import SubHeading from '@/components/Typography/SubHeading';
import Paragraph from '@/components/Typography/Paragraph';

// Style
import style from "./CoursePathTabContent.style";

interface ICoursePathTabContent {
  tabData: {
    id: number,
    title: string,
    description: string,
    picture_desktop?: React.ReactNode,
    picture_mobile?: React.ReactNode,
  }
}

const  CoursePathTabContent = (props: ICoursePathTabContent) => {
  const { title, description, picture_desktop, picture_mobile } = props.tabData;

  // Constants
  const device = useDeviceDetect();
  const isDesktopDevice = device === DEVICE_DESKTOP;

  return (
    <div css={style()}>
      <SubHeading text={title} type="h4"/>
      <Paragraph text={description} type="medium" />
      <div className="">
        {isDesktopDevice ? (
          picture_desktop
        ) : (
          picture_mobile
        )}
      </div>
    </div>
  );
};

export default  CoursePathTabContent;