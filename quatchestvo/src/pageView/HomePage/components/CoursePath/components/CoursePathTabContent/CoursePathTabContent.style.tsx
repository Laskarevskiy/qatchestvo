/** @jsxRuntime classic */
/** @jsx jsx */
import { css } from '@emotion/react';

// Colors
import {  } from '@/common/color';


const style = () => css`
  margin-top: 48px;

  .SubHeading.h4 {
    text-align: center;
    margin-bottom: 24px;
  }

  .Paragraph {
    margin-bottom: 102px;
  }

  svg {
    display: block;
    margin: auto;
  }
`;

export default style;