/** @jsxRuntime classic */
/** @jsx jsx */
import { css } from '@emotion/react';

// Colors
import { secondaryGrey, primaryBlue } from '@/common/color';


const style = () => css`
  position: relative;

  .rec-arrow {
    position: absolute;
    top: 0;
    width: 32px;
    height: 32px;
    min-width: 32px;
    line-height: 32px;
    z-index: 100;
    color: #969BAB;
    background: ${secondaryGrey};
    -webkit-tap-highlight-color: transparent;

    &:hover,
    &:focus {
      background: #E5F2FF !important;
      color: ${primaryBlue} !important;
    }

    &.rec-arrow-right {
      right: calc(50% - 48px);
    }

    &.rec-arrow-left {
      left: calc(50% - 48px);
    }
  }

  .rec-dot:hover {
    box-shadow: 0 0 1px 3px ${primaryBlue} !important;
  }

  .rec-dot_active {
    background: #E5F2FF;
    box-shadow: 0 0 1px 3px ${primaryBlue};

    &:hover {
      box-shadow: 0 0 1px 3px ${primaryBlue} !important;
    }
  }

  .rec-slider-container {
    margin: 0;
  }
`;

export default style;