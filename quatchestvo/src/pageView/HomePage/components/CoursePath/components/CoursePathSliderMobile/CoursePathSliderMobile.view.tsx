/** @jsxRuntime classic */
/** @jsx jsx */
import { jsx } from '@emotion/react';
import Carousel from 'react-elastic-carousel';

// Components
import CoursePathTabContent from '../CoursePathTabContent';

// Style
import style from "./CoursePathSliderMobile.style";

interface ICoursePathSliderMobile {
  coursePathTabContent: Array<any>,
}


const  CoursePathSliderMobile = (props: ICoursePathSliderMobile) => {
  const { coursePathTabContent } = props;

  return (
    <div css={style()}>
      <Carousel isRTL={false} pagination itemsToShow={1}>
        {coursePathTabContent.map(item => (
          <CoursePathTabContent key={item.id} tabData={item} />
        ))}
      </Carousel>
    </div>
  );
};

export default  CoursePathSliderMobile;
