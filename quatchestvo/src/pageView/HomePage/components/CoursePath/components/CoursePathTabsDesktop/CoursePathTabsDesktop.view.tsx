/** @jsxRuntime classic */
/** @jsx jsx */
import React, { useState } from 'react';
import { jsx } from '@emotion/react';

// Components
import CoursePathTabContent from '../CoursePathTabContent';

// Style
import style from "./CoursePathTabsDesktop.style";

// Mock Data
import coursePathTabsList from './course_path_tabs_list.json'

interface ICoursePathTabsDesktop {
  coursePathTabContent: Array<any>,
}

const  CoursePathTabsDesktop = (props: ICoursePathTabsDesktop) => {
  const { coursePathTabContent } = props;

  // State
  const [activeTabId, setActiveTabId] = useState<number>(coursePathTabsList[0].id);

  // Handlers
  const tabClickHandler = (args: { tabId: number }) => {
    const { tabId } = args;

    setActiveTabId(tabId);
  };

  return (
    <>
      <ul css={style()} className="course-path-tabs-list">
        {coursePathTabsList.map(item => {
          const activeTabClassName = item.id === activeTabId ? 'active' : '';

          return (
            <li
              key={item.id}
              onClick={() => tabClickHandler({ tabId: item.id })}
              className={`course-path-tabs-list__item ${activeTabClassName}`}
            >
              {item.title}
            </li>
          )
        })}
      </ul>

      <CoursePathTabContent tabData={coursePathTabContent[activeTabId]} />
    </>
  );
};

export default  CoursePathTabsDesktop;