/** @jsxRuntime classic */
/** @jsx jsx */
import { css } from '@emotion/react';

// Colors
import { secondaryGrey, primaryBlue } from '@/common/color';


const style = () => css`
  &.course-path-tabs-list {
    display: flex;
    flex-wrap: wrap;
    justify-content: center;

    .course-path-tabs-list__item {
      padding: 12px 24px;
      background: ${secondaryGrey};
      border-radius: 12px;
      cursor: pointer;
      font-weight: 600;
      font-size: 16px;
      line-height: 26px;
      color: #969BAB;
      transition: all .6s ease 0s;
      margin-bottom: 16px;

      &:not(:last-of-type) {
        margin-right: 24px;
      }

      &.active {
        background: #E5F2FF;
        color: ${primaryBlue};
      }
    }
  }
`;

export default style;