/** @jsxRuntime classic */
/** @jsx jsx */
import { jsx } from '@emotion/react';

// Components
import SubHeading from '@/components/Typography/SubHeading';
import CoursePathTabsDesktop from './components/CoursePathTabsDesktop';
import CoursePathSliderMobile from './components/CoursePathSliderMobile';

// Hooks
import useDeviceDetect from '@/hooks/useDeviceDetect/index';

// Config
import { DEVICE_DESKTOP } from '@/config';

// Icons
import Icon_course_path_1 from '@/static/images/IconCoursePath1.svg'
import Icon_course_path_2 from '@/static/images/IconCoursePath2.svg'
import Icon_course_path_3 from '@/static/images/IconCoursePath3.svg'
import Icon_course_path_4 from '@/static/images/IconCoursePath4.svg'

// Mobile
import Icon_course_path_mobile_1 from '@/static/images/IconCoursePathMobile1.svg'
import Icon_course_path_mobile_2 from '@/static/images/IconCoursePathMobile2.svg'
import Icon_course_path_mobile_3 from '@/static/images/IconCoursePathMobile3.svg'
import Icon_course_path_mobile_4 from '@/static/images/IconCoursePathMobile4.svg'

// Style
import style from "./CoursePath.style";

const  CoursePath = () => {
  // Constants
  const device = useDeviceDetect();

  const coursePathTabContent = [
    {
      id: 0,
      title: "Как проходят занятия",
      description: "-Количество занятий от 24. Мы учимся скольок нужно группе для того, чтобы все разобрались и поняли материал. -В среднем одно занятие длится 2 часа -Занятия проходят онлайн в зуме. После кажого занятия вы получаете видеозапись. -В начале каждого занятия проходит короткий опрос, для того чтобы понимать как все усвоили материал, и закрепить его. -Время и дни занятий определяется с каждой группой индивидуально, с учетом пожеланий студентов. Цель - чтобы всем было максимально удобно и никто не пропускал занятия.",
      picture_desktop: <Icon_course_path_1 />,
      picture_mobile: <Icon_course_path_mobile_1 />,
    },
    {
      id: 1,
      title: "Самостоятельная работа",
      description: "-Мы даем материалы по которым необходимо готовиться к каждому занятию, для того чтобы лекция усваивалась легче -После каждого занятия есть домашние практические работы, построеные согласно активностям, которые вы будете делать на работе. А управление домашними заданиями происходит в JIRA для того, чтобы вы с самых ранних этапов учились работать с системами в которых будете работать в новой роли. -На сделанную домашнюю работу вы получаете подробные комментарии от ментора и переделываете пока работа не будет сделана хорошо. -Также вы получаете авторскую \"методичку\" со всей теорией тестирования и другими важными темами, по которой сможете удобно учить материал.",
      picture_desktop: <Icon_course_path_2 />,
      picture_mobile: <Icon_course_path_mobile_2 />,
    },
    {
      id: 2,
      title: "Знания",
      description: "-Вы будете тестировать несколько версий программного обеспечения созданного специально для курсов, а также реальные проекты -Тесты будут писаться в Testrail а баги заводиться в Jira, где вы будете следить за статусом исправления дефектов и в результате проверять исправность -Вы на практике будете тестировать API с использованием  Postman, Swagger и Jmeter. Работать с GIT, terminal, базами данных а также инструментами тестирования мобильных устройств, такими как Android studio и Charles. ",
      picture_desktop: <Icon_course_path_3 />,
      picture_mobile: <Icon_course_path_mobile_3 />,
    },
    {
      id: 3,
      title: "Собеседование",
      description: "-Мы отдельно много времени уделяем этому этапу -Для вас будут проводить лекцию по подготовке к собеседованию специалисты по рекрутенку из BazaIT -Будет общее техническое интервью, а после персональные интервью с расставлением акцентов на что стоит обратить внимание -Мы вместе подготовим красивое резюме в котором будет минимум 4 сертификата подтверждающих ваши знания -А также разберем алгоритмы поиска работы",
      picture_desktop: <Icon_course_path_4 />,
      picture_mobile: <Icon_course_path_mobile_4 />,
    }
  ];

  return (
    <section css={style()}>
      <SubHeading text="Как происходит обучение" type="h2" />

      {device === DEVICE_DESKTOP ? (
        <CoursePathTabsDesktop coursePathTabContent={coursePathTabContent} />
      ) : (
        <CoursePathSliderMobile coursePathTabContent={coursePathTabContent} />
      )}

    </section>
  );
};

export default  CoursePath;