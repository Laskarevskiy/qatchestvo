/** @jsxRuntime classic */
/** @jsx jsx */
import { jsx } from "@emotion/react";
import ScrollAnimation from "react-animate-on-scroll";

// Components
import SubHeading from "@/components/Typography/SubHeading";
import Paragraph from "@/components/Typography/Paragraph";

// Icons
import IconAndroidStudio from '@/static/images/learnTechnologies/IconAndroidStudio.svg';
import IconCharles from '@/static/images/learnTechnologies/IconCharles.svg';
import IconCodeceptJs from '@/static/images/learnTechnologies/IconCodeceptJs.svg';
import IconCss from '@/static/images/learnTechnologies/IconCss.svg';
import IconGit from '@/static/images/learnTechnologies/IconGit.svg';
import IconHtml from '@/static/images/learnTechnologies/IconHtml.svg';
import IconJira from '@/static/images/learnTechnologies/IconJira.svg';
import IconJmetr from '@/static/images/learnTechnologies/IconJmetr.svg';
import IconPostman from '@/static/images/learnTechnologies/IconPostman.svg';
import IconSQL from '@/static/images/learnTechnologies/IconSQL.svg';
import IconSwagger from '@/static/images/learnTechnologies/IconSwagger.svg';
import IconTerminal from '@/static/images/learnTechnologies/IconTerminal.svg';
import IconTestRail from '@/static/images/learnTechnologies/IconTestRail.svg';
import IconVSCode from '@/static/images/learnTechnologies/IconVSCode.svg';

// Style
import style from "./LearnTechnology.style";

const LearnTechnology = () => {
  const learnTechnologyData = [
    {
      "icon": <IconSQL />,
      "title": "SQL"
    },
    {
      "icon": <IconGit />,
      "title": "Git"
    },
    {
      "icon": <IconTerminal />,
      "title": "Terminal"
    },
    {
      "icon": <IconPostman />,
      "title": "Postman"
    },
    {
      "icon": <IconJmetr />,
      "title": "JMetr"
    },
    {
      "icon": <IconVSCode />,
      "title": "VScode"
    },
    {
      "icon": <IconAndroidStudio />,
      "title": "Android studio"
    },
    {
      "icon": <IconHtml />,
      "title": "HTML5"
    },
    {
      "icon": <IconSwagger />,
      "title": "Swagger"
    },
    {
      "icon": <IconCss />,
      "title": "Сss3"
    },
    {
      "icon": <IconTestRail />,
      "title": "TestRail"
    },
    {
      "icon": <IconJira />,
      "title": "Jira"
    },
    {
      "icon": <IconCharles />,
      "title": "Charles"
    },
    {
      "icon": <IconCodeceptJs />,
      "title": "CodeceptJS"
    }
  ]

  return (
    <ScrollAnimation animateIn="fadeIn">
      <section css={style()}>
        <SubHeading text="Изучаемые технологии" type="h4" />
        <div className="technology-list-container">
          <ul className="technology-list">
            {learnTechnologyData.map((item) => (
              <li key={item.title} className="technology-list__item">
                {item.icon}
                <Paragraph text={item.title} type="small" />
              </li>
            ))}
          {learnTechnologyData.map((item) => (
              <li key={item.title} className="technology-list__item">
                {item.icon}
                <Paragraph text={item.title} type="small" />
              </li>
            ))}
          </ul>
        </div>
      </section>
    </ScrollAnimation>
  );
};

export default LearnTechnology;
