/** @jsxRuntime classic */
/** @jsx jsx */
import { css } from '@emotion/react';

// Config
import {
  mediaMobileTablet,
  mediaDesktop,
} from '@/config';

const style = () => css`
  /* Common style */
  @keyframes infinityScroll {
    0% {
        transform: translate(0, 0);
    }
    100% {
        transform: translate(-50%, 0);
    }
  }

  .technology-list-container {
    position: relative;
    height: 104px;
    overflow: hidden;
  }

  .technology-list {
    display: flex;
    position: absolute;
    top: 0px;
    left: 0px;
    overflow: hidden;
    white-space: nowrap;
    animation: infinityScroll 40s linear infinite;

    .technology-list__item  {
      display: flex;
      flex-direction: column;
      align-items: center;

      .Paragraph {
        font-weight: 700;
        margin-top: 12px;
      }
    }
  }

  /* Style mobile => tablet */
  ${mediaMobileTablet} {
    .SubHeading {
      margin-bottom: 24px;
    }

    .technology-list__item  {
      margin-right: 38px;
    }
  }

  /* Style desktop => */
  ${mediaDesktop} {
    .SubHeading {
      text-align: center;
      margin-bottom: 44px;
    }

    .technology-list__item  {
      margin-right: 76px;
    }
  }
`;

export default style;