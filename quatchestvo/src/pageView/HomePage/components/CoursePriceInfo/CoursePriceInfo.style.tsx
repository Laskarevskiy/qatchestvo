/** @jsxRuntime classic */
/** @jsx jsx */
import { css } from '@emotion/react';

// Colors
import { primaryWhite } from '@/common/color';

// Config
import {
  mediaMobileTablet,
  mediaDesktop,
} from '@/config';

const style = () => css`
  /* Common style */
  color: ${primaryWhite};
  text-align: center;

  .course-price-info-list__item {
    max-width: 255px;
  }

  /* Style mobile => tablet */
  ${mediaMobileTablet} {
    .SubHeading.h2 {
      margin-bottom: 40px;
    }

    .course-price-info-list__item {
      margin: 0 auto;

      &:not(:last-of-type) {
        margin-bottom: 40px;
      }
    }
  }

  /* Style desktop => */
  ${mediaDesktop} {
    .SubHeading.h2 {
      margin-bottom: 48px;
    }

    .course-price-info-list {
      display: flex;
      flex-wrap: wrap;
      justify-content: space-between;

      .course-price-info-list__item {
        display: flex;

        &:not(:last-of-type):after {
          content: '';
          display: inline;
          border-right: 1px solid ${primaryWhite};
          padding: 0 16px;
        }
      }
    }

    @media (max-width: 1110px) {
      .course-price-info-list {
        justify-content: normal;
        flex-wrap: nowrap;

        .course-price-info-list__item {
          padding: 0 16px;
        }
      }
    }
  }
`;

export default style;