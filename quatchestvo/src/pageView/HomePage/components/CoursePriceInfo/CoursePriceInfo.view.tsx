/** @jsxRuntime classic */
/** @jsx jsx */
import { jsx } from '@emotion/react';
import ScrollAnimation from 'react-animate-on-scroll';

// Components
import SubHeading from '@/components/Typography/SubHeading';
import Paragraph from '@/components/Typography/Paragraph';

// Style
import style from "./CoursePriceInfo.style";

const  CoursePriceInfo = () => {
  const coursePriceInfoData = [
    {
      title: 'от 24 занятий',
      subTitle: 'Занятия проходят в онлайн режиме',
    },
    {
      title: '2 часа',
      subTitle: 'Длительность одного занятия',
    },
    {
      title: '$600 / курс',
      subTitle: 'или Оплата частями = 2х300$',
    },
    {
      title: '4 - 6 студентов',
      subTitle: 'Уделяется действительно много времени для каждого',
    },
  ];

  return (
    <ScrollAnimation animateIn='fadeIn'>
      <section css={style()}>
        <SubHeading text="Стоимость обучения" type="h2" />

        <ul className="course-price-info-list">
          {coursePriceInfoData.map(item => (
            <li key={item.title} className="course-price-info-list__item">
              <div>
                <SubHeading text={item.title} type="h4" />
                <Paragraph text={item.subTitle} type="medium" />
              </div>
            </li>
          ))}
        </ul>
      </section>
    </ScrollAnimation>
  );
};

export default  CoursePriceInfo;
