/** @jsxRuntime classic */
/** @jsx jsx */
import { jsx } from '@emotion/react';

// Components
import SubHeading from '@/components/Typography/SubHeading';
import Paragraph from '@/components/Typography/Paragraph';

// Icons
import IconPeoples from '@/static/images/IconPeoples.svg';
import IconCalendar from '@/static/images/IconCalendar.svg';
import IconThought from '@/static/images/IconThought.svg';
import IconRise from '@/static/images/IconRise.svg';
import IconBriefcase from '@/static/images/IconBriefcase.svg';
import IconRound from '@/static/images/IconRound.svg';

// Style
import style from "./CourseBenefits.style";

const  CourseBenefits = () => {
  const courseBenefitsData = [
    {
      icon: <IconPeoples />,
      title: 'Маленькие группы',
      text: 'Обучение происходит в маленькой группе (4-6 человек), что позволяет уделять действительно много времени для каждого',
    },
    {
      icon: <IconCalendar />,
      title: 'Гибкий график занятий',
      text: 'Мы формируем график занятий вместе с группой, чтоб всем было максимально удобно',
    },
    {
      icon: <IconThought />,
      title: 'Много повторяем',
      text: 'Мы не двигаемся дальше по материалам пока все члены группы не поймут материал на достаточном уровне',
    },
    {
      icon: <IconRise />,
      title: 'Готовимся к собеседованию',
      text: 'Мы итерируем с подготовкой к собеседованию, что позволяет чувствовать себя уверенно с первого интервью',
    },
    {
      icon: <IconBriefcase />,
      title: 'Рекомендация компаниям',
      text: 'Мы не гарантируем трудоустройство, но благодаря большому количеству контактов всячески способствую организации интервью для вас',
    },
    {
      icon: <IconRound />,
      title: 'Повторяем еще :)',
      text: 'Мы не привязаны к окончательной дате, а значит будем учиться пока все не будут знать достаточно для поиска работы',
    },
  ];

  return (
    <section css={style()}>
      <SubHeading text="Преимущества обучения" type="h2" />

      <ul className="course-benefits-list">
        {courseBenefitsData.map(item => (
          <li key={item.title} className="course-benefits-list__item">
            {item.icon}
            <SubHeading text={item.title} type="h5"/>
            <Paragraph text={item.text} type="medium" />
          </li>
        ))}
      </ul>
    </section>
  );
};

export default  CourseBenefits;