/** @jsxRuntime classic */
/** @jsx jsx */
import { css } from '@emotion/react';

// Colors
import { primaryWhite } from '@/common/color';

// Config
import {
  mediaMobileTablet,
  mediaDesktop,
} from '@/config';

const style = () => css`
  /* Common style */
  .SubHeading.h2 {
    text-align: center;
  }

  .course-benefits-list__item {
    display: flex;
    flex-direction: column;
    align-items: center;

    .SubHeading {
      text-align: center;
      margin-bottom: 8px;
    }

    .Paragraph {
      text-align: center;
    }
  }

  /* Style mobile => tablet */
  ${mediaMobileTablet} {
    .SubHeading.h2 {
      margin-bottom: 40px;
    }

    .course-benefits-list__item {
      svg {
        margin-bottom: 16px;
      }

      &:not(:last-of-type) {
        margin-bottom: 40px;
      }
    }
  }

  /* Style desktop => */
  ${mediaDesktop} {
    .SubHeading.h2 {
      margin-bottom: 48px;
    }

    .course-benefits-list {
      display: grid;
      grid-template-columns: minmax(190px, 350px) minmax(190px, 350px) minmax(190px, 350px);
      grid-column-gap: 30px;
      grid-row-gap: 64px;

      .course-benefits-list__item {
        svg {
          margin-bottom: 24px;
        }
      }
    }
  }
`;

export default style;