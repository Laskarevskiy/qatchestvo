/** @jsxRuntime classic */
/** @jsx jsx */
import { jsx } from '@emotion/react';
import ScrollAnimation from 'react-animate-on-scroll';

// Components
import SubHeading from '@/components/Typography/SubHeading';
import Paragraph from '@/components/Typography/Paragraph';
import FreeConsultationButton from '@/components/FreeConsultationButton';

// Style
import style from './FreeConsultation.style';

const FreeConsultation = () => {

  return (
    <ScrollAnimation animateIn='fadeIn'>
      <div css={style()}>
          <span className="free-consultation-logo" />
          <div>
            <SubHeading
              text="Бесплатная консультация по IT профессиям"
              type="h4"
            />
            <Paragraph
              text="Если вы не уверены на счет того, что хотите быть именно QA тестеровщиком, мы можем бесплатно вам рассказать какие профессии в IT бывают, какие функции они выполняют, чтоб вы могли сделать правильный выбор."
              type="small"
            />
            <FreeConsultationButton />
          </div>
      </div>
    </ScrollAnimation>
  );
};

export default FreeConsultation;
