/** @jsxRuntime classic */
/** @jsx jsx */
import { css } from '@emotion/react';

// Colors
import { secondaryGrey } from '@/common/color';

// Config
import {
  mediaMobileTablet,
  mediaDesktop,
} from '@/config';

const style = () => css`
  /* Common style */
  background: ${secondaryGrey};
  border-radius: 16px;
  padding: 32px 16px;

  .free-consultation-logo {
      display: block;
      background-position: center;
      background-repeat: no-repeat;
  }

  .Paragraph {
    margin-bottom: 32px;
  }

  /* Style mobile => tablet */
  ${mediaMobileTablet} {
    .free-consultation-logo {
      background-image: url('/static/images/Envelope.svg');
      min-width: 164px;
      width: 164px;
      min-height: 150px;
      height: 150px;
      margin: 0 auto 32px;
    }

    .SubHeading {
      margin-bottom: 24px;
    }

    .Button {
      width: 100%;
    }
  }

  /* Style desktop => */
  ${mediaDesktop} {
    display: flex;
    padding: 48px 80px;

    .free-consultation-logo {
      background-image: url('/static/images/Envelope.svg');
      min-width: 300px;
      width: 300px;
      min-height: 274px;
      height: 274px;
      margin-right: 103px;
    }

    .SubHeading {
      margin-bottom: 32px;
    }
  }
`;

export default style;