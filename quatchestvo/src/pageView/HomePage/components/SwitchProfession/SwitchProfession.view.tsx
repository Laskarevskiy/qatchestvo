/** @jsxRuntime classic */
/** @jsx jsx */
import { jsx } from '@emotion/react';
import ScrollAnimation from 'react-animate-on-scroll';

// Components
import SubHeading from '@/components/Typography/SubHeading';
import Paragraph from '@/components/Typography/Paragraph';

// Style
import style from './SwitchProfession.style';

const SwitchProfession = () => {

  const switchersData = [
    {
      number: '1',
      title: 'Вы - Новичок',
      description: 'который хочет в IT здесь и сейчас, без длительного и сложного обучения программированию',
    },
    {
      number: '2',
      title: 'Вы хотите поменять сферу деятельности',
      description: 'переучиться и получить перспективную IT-профессию',
    },
    {
      number: '3',
      title: 'Вы - Самоучка',
      description: 'которому нужно систематизировать знания',
    },
    {
      number: '4',
      title: 'Вы разработчик',
      description: 'который хочет детально разбираться в смежной отрасли',
    },
  ];

  return (
    <div css={style()}>
      <SubHeading text="Для кого подойдет этот курс" type="h2" />

      <ul className="switch-profession-list">
        {switchersData.map(item => (
          <li key={item.number} className="switch-profession-list__item">
            <ScrollAnimation animateIn='fadeIn'>
            <span className="switch-profession-list__item--number">{item.number}</span>
            </ScrollAnimation>
            <div>
              <SubHeading text={item.title} type="h5" />
              <Paragraph text={item.description} type="small" />
            </div>
          </li>
        ))}
      </ul>
    </div>
  );
};

export default SwitchProfession;
