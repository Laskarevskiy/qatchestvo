/** @jsxRuntime classic */
/** @jsx jsx */
import { css } from '@emotion/react';

// Colors
import { greyLight } from '@/common/color';

// Config
import {
  mediaMobileTablet,
  mediaDesktop,
} from '@/config';

const style = () => css`
  /* Common style */
  .switch-profession-list {
      display: flex;

    &__item {
      display: flex;

      .switch-profession-list__item--number {
        display: flex;
        align-items: center;
        justify-content: center;
        border-radius: 50%;
        background: ${greyLight};
        color: #969BAB;
      }
    }
  }

  /* Style mobile => tablet */
  ${mediaMobileTablet} {
    .SubHeading.h2 {
      margin-bottom: 24px;
    }

    .switch-profession-list {
      display: flex;
      flex-direction: column;

      &__item {
        display: flex;

        &:not(:last-of-type) {
          margin-bottom: 24px;
        }

        .switch-profession-list__item--number {
          font-size: 28px;
          line-height: 40px;
          font-weight: 700;

          width: 56px;
          min-width: 56px;
          height: 56px;
          min-height: 56px;
          padding: 8px 18px;
          margin-right: 16px;
        }

        .SubHeading {
          margin-bottom: 2px;
        }
      }
    }
  }

  /* Style desktop => */
  ${mediaDesktop} {
    .SubHeading.h2 {
      margin-bottom: 48px;
    }

    .switch-profession-list {
      flex-wrap: wrap;
      margin: 0 10px;

      &__item {
        width: 50%;
        margin: 0 -10px;

        &:not(:nth-child(n+3)) {
          margin-bottom: 48px;
        }

        &:nth-child(odd) {
          margin-right: 30px;
        }

        .switch-profession-list__item--number {
          font-size: 40px;
          line-height: 54px;
          font-weight: 800;

          width: 72px;
          min-width: 72px;
          height: 72px;
          min-height: 72px;
          margin-right: 40px;
        }

        .SubHeading {
          margin-bottom: 8px;
        }
      }
    }
  }
`;

export default style;