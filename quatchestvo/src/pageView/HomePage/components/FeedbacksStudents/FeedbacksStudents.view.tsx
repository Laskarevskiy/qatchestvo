/** @jsxRuntime classic */
/** @jsx jsx */
import { useState } from 'react';
import { jsx } from '@emotion/react';
import ScrollAnimation from 'react-animate-on-scroll';
import dynamic from 'next/dynamic';

// Components
import SubHeading from '@/components/Typography/SubHeading';
import Paragraph from '@/components/Typography/Paragraph';
import FeedBackCard from '@/components/FeedBackCard';
import Button from '@/components/Button';
import StudentsContactsList from './components/StudentsContactsList';

// Icons
import IconArrowRightFilledBlue from '@/static/images/IconArrowRightFilledBlue.svg';

// Style
import style from "./FeedbacksStudents.style";

// Dynamic Import Components
const Modal = dynamic(() => import('@/components/Modal'), { ssr: false });

// Mock Data
import feedbacksStudents_1 from './feedbacks_students_1.json';
import feedbacksStudents_2 from './feedbacks_students_2.json';

const  FeedbacksStudents = () => {
  // State
  const [showModalStudentsFeedBacks, setShowModalStudentsFeedBacks] = useState(false);

  // Handlers
  const showModalStudentsFeedBacksHandler = () => {
    setShowModalStudentsFeedBacks(prevState => !prevState);
  };

  return (
    <ScrollAnimation animateIn='fadeIn'>
      <div css={style()}>
        {/* Left block */}
        <div className="feedbacks-container">
          <SubHeading type="h2" >
            <span className="feedback-title">Отзывы</span>
            студентов курсов
          </SubHeading>
          <Paragraph
            type="medium"
            weight="600"
            customClassName="feedback-sub-title"
          >
            Более 80%
            <span className="feedback-sub-title__weight-light">студентов</span>
            нашли работу
          </Paragraph>

          <ul className="feedbacks-students-list">
            {feedbacksStudents_1.map(item => (
              <FeedBackCard
                key={item.id}
                feedbackDescription={item.feedbackDescription}
                userLinkedinLink={item.userLinkedinLink}
                userName={item.userName}
                userPosition={item.userPosition}
              />
            ))}
          </ul>

        </div>

        {/* Right block */}
        <div className="feedbacks-container">
          <ul className="feedbacks-students-list">
            {feedbacksStudents_2.map(item => (
              <FeedBackCard
                key={item.id}
                feedbackDescription={item.feedbackDescription}
                userLinkedinLink={item.userLinkedinLink}
                userName={item.userName}
                userPosition={item.userPosition}
              />
            ))}
          </ul>
          <div className="feedbacks-students-action-container">
            <Button
              heightButton="48"
              text="Узнать, где бывшие студенты работают"
              textSize="text-small"
              type="primary"
              onClick={showModalStudentsFeedBacksHandler}
            />

            <div className="dou-link-container">
              <a
                href="https://dou.ua/"
                target="_blank"
                rel="noreferrer"
              >
                Отзывы выпускников на DOU.UA
              </a>
              <IconArrowRightFilledBlue />
            </div>
          </div>
        </div>
      </div>

      {showModalStudentsFeedBacks && (
        <Modal closeModalHandler={showModalStudentsFeedBacksHandler}>
          <StudentsContactsList />
        </Modal>
      )}
    </ScrollAnimation>
  );
};

export default  FeedbacksStudents;