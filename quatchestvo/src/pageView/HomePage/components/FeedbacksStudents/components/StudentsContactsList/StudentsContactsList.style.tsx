/** @jsxRuntime classic */
/** @jsx jsx */
import { css } from '@emotion/react';

// Config
import {
  mediaMobileTablet,
  mediaDesktop,
} from '@/config';

const style = () => css`
  /* Style mobile => tablet */
  ${mediaMobileTablet} {
    .SubHeading.h4 {
      margin-bottom: 8px;
    }
  }

  /* Style desktop => */
  ${mediaDesktop} {
    .SubHeading.h4 {
      margin-bottom:24px;
    }
  }
`;

export default style;