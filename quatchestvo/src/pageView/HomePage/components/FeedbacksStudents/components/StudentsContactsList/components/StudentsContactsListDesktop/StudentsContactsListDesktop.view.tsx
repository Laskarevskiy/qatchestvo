/** @jsxRuntime classic */
/** @jsx jsx */
import React from 'react';
import { jsx } from "@emotion/react";

// Components
import Caption from '@/components/Typography/Caption';
import LinkedinLink from '@/components/LinkedinLink';

// Icons
import IconTelegram from '@/static/images/IconTelegram.svg';

// Style
import style from "./StudentsContactsListDesktop.style";

interface IStudentsContactsListDesktop {
  studentsContactsListData: Array<any>,
}

const StudentsContactsListDesktop = (props: IStudentsContactsListDesktop) => {
  const { studentsContactsListData } = props;

  return (
    <div css={style()}>
      <table className="students-contacts-list-header-table">
        <tr>
          <Caption text="Имя и Фамилия" type="medium" />
        </tr>
        <tr>
          <Caption text="Компания" type="medium" />
        </tr>
        <tr>
          <Caption text="Способ связи" type="medium" />
        </tr>
        <tr>
          <Caption text="Linkedin" type="medium" />
        </tr>
      </table>

      <ul>
        {studentsContactsListData.map(item => (
          <li key={item.id} className="students-contacts-list-content-table">
            <span>
              <Caption text={item.name} type="medium" />
            </span>
            <span>
              <Caption text={item.company} type="medium" />
            </span>
            <a href={item.connectionTelegramLink}>
              <IconTelegram />
            </a>
            <LinkedinLink isLink link={item.linkedinLink} name={item.linkedinLinkName} />
          </li>
        ))}
      </ul>
    </div>
  );
};

export default StudentsContactsListDesktop;
