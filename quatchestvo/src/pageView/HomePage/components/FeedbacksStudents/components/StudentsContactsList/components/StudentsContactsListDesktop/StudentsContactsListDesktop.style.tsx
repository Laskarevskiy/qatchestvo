/** @jsxRuntime classic */
/** @jsx jsx */
import { css } from '@emotion/react';

const style = () => css`
  .students-contacts-list-header-table {
    display: grid;
    grid-template-columns: minmax(124px, 124px) minmax(124px, 124px) minmax(124px, 124px) 1fr;
    grid-column-gap: 16px;
    align-items: center;
    justify-items: center;
    margin-bottom: 24px;
  }

  .students-contacts-list-content-table {
    display: grid;
    grid-template-columns: minmax(124px, 124px) minmax(124px, 124px) minmax(124px, 124px) 1fr;
    grid-column-gap: 16px;
    align-items: center;
    justify-items: center;
    margin-bottom: 32px;

    &:last-of-type {
      margin-bottom: 0;
    }

    .Caption.medium {
      color: #18191F !important;
    }
  }
`;

export default style;