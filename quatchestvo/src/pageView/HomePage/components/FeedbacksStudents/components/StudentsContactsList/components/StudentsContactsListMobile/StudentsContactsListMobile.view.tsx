/** @jsxRuntime classic */
/** @jsx jsx */
import { jsx } from "@emotion/react";

// Components
import Caption from '@/components/Typography/Caption';
import LinkedinLink from '@/components/LinkedinLink';

// Icons
import IconTelegram from '@/static/images/IconTelegram.svg';

// Style
import style from "./StudentsContactsListMobile.style";

interface IStudentsContactsListMobile {
  studentsContactsListData: Array<any>,
}

const StudentsContactsListMobile = (props: IStudentsContactsListMobile) => {
  const { studentsContactsListData } = props;

  return (
    <div css={style()}>
      <Caption text="Имя и Фамилия / Компания / Способ связи / Linkedin" type="medium" />

      <ul>
        {studentsContactsListData.map(item => (
          <li key={item.id} className="students-contacts-list-content-table">
            <span>
              <Caption text={`${item.name}, ${item.company}`} type="medium" />
            </span>
            <a href={item.connectionTelegramLink}>
              <IconTelegram />
            </a>
            <LinkedinLink isLink link={item.linkedinLink} />
          </li>
        ))}
      </ul>
    </div>
  );
};

export default StudentsContactsListMobile;
