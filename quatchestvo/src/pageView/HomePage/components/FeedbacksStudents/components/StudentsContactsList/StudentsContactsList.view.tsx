/** @jsxRuntime classic */
/** @jsx jsx */
import { jsx } from "@emotion/react";

// Hooks
import useDeviceDetect from '@/hooks/useDeviceDetect/index';

// Config
import { DEVICE_DESKTOP } from '@/config';

// Components
import SubHeading from "@/components/Typography/SubHeading";
import StudentsContactsListDesktop from './components/StudentsContactsListDesktop';
import StudentsContactsListMobile from './components/StudentsContactsListMobile';

// Style
import style from "./StudentsContactsList.style";

// Mock Data
import studentsContactsListData from './students_contacts_list_data.json';

const StudentsContactsList = () => {
  // Constants
  const device = useDeviceDetect();
  const isDesktopDevice = device === DEVICE_DESKTOP;

  return (
    <div css={style()}>
      <SubHeading text="Бывшие студенты курса" type="h4" />

      {isDesktopDevice ? (
        <StudentsContactsListDesktop studentsContactsListData={studentsContactsListData} />
      ) : (
        <StudentsContactsListMobile studentsContactsListData={studentsContactsListData} />
      )}
    </div>
  );
};

export default StudentsContactsList;
