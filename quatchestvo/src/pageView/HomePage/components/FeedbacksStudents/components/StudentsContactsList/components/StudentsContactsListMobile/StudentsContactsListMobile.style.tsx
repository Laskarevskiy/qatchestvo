/** @jsxRuntime classic */
/** @jsx jsx */
import { css } from '@emotion/react';

const style = () => css`
  .Caption.medium {
    text-align: start;
    margin-bottom: 8px;
  }

  .students-contacts-list-content-table {
    display: grid;
    grid-template-columns: 1fr minmax(24px, 24px) minmax(24px, 24px);
    grid-column-gap: 16px;
    align-items: center;
    margin-bottom: 16px;

    &:last-of-type {
      margin-bottom: 0;
    }

    .Caption.medium {
      color: #18191F !important;
    }
  }
`;

export default style;