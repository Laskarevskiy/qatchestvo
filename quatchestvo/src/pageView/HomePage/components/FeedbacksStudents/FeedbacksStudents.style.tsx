/** @jsxRuntime classic */
/** @jsx jsx */
import { css } from '@emotion/react';

// Colors
import { primaryBlue } from '@/common/color';

// Config
import {
  mediaMobileTablet,
  mediaDesktop,
} from '@/config';

const style = () => css`
  /* Common style */

  .SubHeading.h2 {
    margin-bottom: 8px;
  }

  .feedback-title {
    color: ${primaryBlue};
    margin: 0 8px 8px 0;
  }

  .feedback-sub-title__weight-light {
    font-weight: 400;
    margin: 0 8px;
  }

  .Button.primary {
    margin-bottom: 24px;
  }

  .dou-link-container {
    display: flex;
    align-items: center;
    justify-content: center;
    color: ${primaryBlue};

    .IconArrowRightFilledBlue_svg__icon {
      margin-left: 10px;
    }
  }

  /* Style mobile => tablet */
  ${mediaMobileTablet} {
    display: flex;
    flex-direction: column;

    .feedback-sub-title {
      margin-bottom: 32px;
    }

    .feedbacks-card {
      margin-bottom: 32px;

      &:last-of-type {
        margin-bottom: 24px;
      }
    }

    .Button.primary {
      width: 100%;
    }
  }

  /* Style desktop => */
  ${mediaDesktop} {
    display: flex;
    justify-content: center;

    .feedback-sub-title {
      margin-bottom: 44px;
    }

    .feedbacks-container {

      .feedbacks-card {
        margin-bottom: 30px;

        &:nth-child(2) {
          max-width: 350px;
        }
      }

      /* Left block */
      &:first-of-type {
        max-width: 445px;
        margin-right: 30px;

        .feedbacks-students-list {
          display: flex;
          flex-direction: column;
          align-items: end;
        }
      }

      /* Right block */
      &:last-of-type {
        margin-top: 48px;
      }
    }

    .feedbacks-students-action-container {
      display: inline-block;

      .Button.primary {
        min-width: 344px;
      }
    }
  }
`;

export default style;