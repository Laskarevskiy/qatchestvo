/** @jsxRuntime classic */
/** @jsx jsx */
import { css } from '@emotion/react';

// Config
import {
  mediaMobileTablet,
  mediaDesktop,
} from '@/config';

const style = () => css`
  /* Common style */
  .after-course-list {
    list-style: disc;
    margin-left: 16px;

    .after-course-list__item {
      margin-bottom: 16px;

      &:last-child {
        margin-bottom: 0;
      }
    }
  }

  .Button {
    margin-bottom: 40px;
  }

  .SubHeading.h4 {
    margin-bottom: 16px;
  }

  .course-partners-list {
    margin-top: 32px;
  }

  .after-course-phone-wrap {
    svg {
      width: 100%;
    }
  }

  /* Style mobile => tablet */
  ${mediaMobileTablet} {
    .SubHeading.h2 {
      margin-bottom: 24px;
    }

    .after-course-list-wrap {
      margin-bottom: 40px;

      .Button {
        width: 100%;
      }

      .after-course-employment-container {
        margin-bottom: 40px;
      }
    }

    .after-course-list {
      margin-bottom: 40px;
    }
  }

  /* Style desktop => */
  ${mediaDesktop} {
    .SubHeading.h2 {
      margin-bottom: 48px;
    }

    .after-course-container {
      display: flex;
      justify-content: space-between;
    }

    .after-course-list-wrap {
      max-width: 672px;

      .after-course-employment-container {
        &:not(:last-of-type) {
          margin-bottom: 54px;
        }
      }
    }

    .after-course-list {
      margin-bottom: 48px;
      max-width: 428px;
    }
  }
`;

export default style;