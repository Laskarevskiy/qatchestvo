/** @jsxRuntime classic */
/** @jsx jsx */
import { css } from '@emotion/react';

// Config
import {
  mediaMobileTablet,
  mediaDesktop,
} from '@/config';

const style = () => css`
  /* Common style */
  img {
    display: block;
    margin: auto;
    max-width: 420px;
    width: 100%;
  }

  /* Style mobile => tablet */
  ${mediaMobileTablet} {
    .SubHeading.h5 {
      margin-bottom: 24px;
    }
  }

  /* Style desktop => */
  ${mediaDesktop} {
    .SubHeading.h5 {
      margin-bottom: 48px;
    }
  }
`;

export default style;