/** @jsxRuntime classic */
/** @jsx jsx */
import { jsx } from '@emotion/react';

// Components
import SubHeading from '@/components/Typography/SubHeading';

// Style
import style from "./ExampleResumeStudent.style";

const  ExampleResumeStudent = () => {

  return (
    <div css={style()}>
      <SubHeading text="Как будет выглядеть твое резюме" type="h5" />

      <img src="./static/images/example-resume-student.png" alt="" />
    </div>
  );
};

export default  ExampleResumeStudent;