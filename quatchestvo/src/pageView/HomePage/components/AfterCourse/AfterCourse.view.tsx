/** @jsxRuntime classic */
/** @jsx jsx */
import { jsx } from '@emotion/react';
import React, { useState } from 'react';
import ScrollAnimation from 'react-animate-on-scroll';
import dynamic from 'next/dynamic';


// Components
import SubHeading from '@/components/Typography/SubHeading';
import Paragraph from '@/components/Typography/Paragraph';
import Button from '@/components/Button';

// Icons
import IphoneX from '@/static/images/IphoneX.svg';
import IconBazaIT from '@/static/images/IconBazaIT.svg';

// Style
import style from './AfterCourse.style';

// Dynamic Import Components
const Modal = dynamic(() => import('@/components/Modal'), { ssr: false });
const ExampleResumeStudent = dynamic(() => import('./components/ExampleResumeStudent'), { ssr: false });

const AfterCourse = () => {
  // State
  const [toggleModalExampleResume, setToggleModalExampleResume] = useState(false);
  const afterCourseListData = [
    {
      title: 'Анализировать требования',
    },
    {
      title: 'Проектировать тестовую документацию',
    },
    {
      title: 'Проводить сценарное и исследовательское тестирование',
    },
    {
      title: 'Заводить баг репорты и работать с багтрекинговыми системами',
    },
    {
      title: 'Тестировать клиент-серверные приложения (web и mobile)',
    },
    {
      title: 'Работать с базами данных, терминалом и git.',
    },
  ];

  // Handlers
  const exampleResumeButtonClickHandler = () => {
    setToggleModalExampleResume(prevState => !prevState);
  };

  return (
    <>
      <ScrollAnimation animateIn='fadeIn'>
        <section css={style()}>
          <SubHeading text="После окночания курса вы сможете" type="h2" />

          <div className="after-course-container">
            <div className="after-course-list-wrap">
              <ul className="after-course-list">
                {afterCourseListData.map(item => (
                  <li key={item.title} className="after-course-list__item">
                    <Paragraph text={item.title} type="medium" />
                  </li>
                ))}
              </ul>

              <Button
                text="Как будет выглядеть твое резюме"
                textSize="text-small"
                type="primary"
                onClick={exampleResumeButtonClickHandler}
              />

              <div className="after-course-employment-container">
                <SubHeading text="Трудоустройство" type="h4" />
                <Paragraph
                  text="Мы итерируем с подготовкой к собеседованию, что позволяет чувствовать себя уверенно с первого интервью. Мы не гарантируем трудоустройство но благодаря большому количеству контактов всячески способствуем организации интервью для вас"
                  type="medium"
                />
              </div>

              <div className="after-course-employment-container">
                <SubHeading text="Партнеры по трудоустройству" type="h4" />
                <Paragraph
                  text="BazaIT– профессиональные рекрутеры, которые помогут в подготовке к собеседованию и поиске работы"
                  type="medium"
                />

                <ul className="course-partners-list">
                  <li className="course-partners-list__item">
                    <IconBazaIT />
                  </li>
                </ul>
              </div>
            </div>

            <div className="after-course-phone-wrap">
              <IphoneX />
            </div>
          </div>
        </section>
      </ScrollAnimation>

      {toggleModalExampleResume && (
        <Modal closeModalHandler={exampleResumeButtonClickHandler}>
            <ExampleResumeStudent />
        </Modal>
      )}
    </>
  );
};

export default AfterCourse;
