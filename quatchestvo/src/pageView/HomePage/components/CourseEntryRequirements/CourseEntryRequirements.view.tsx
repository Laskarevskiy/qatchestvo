/** @jsxRuntime classic */
/** @jsx jsx */
import { jsx } from '@emotion/react';
import ScrollAnimation from 'react-animate-on-scroll';

// Components
import SubHeading from '@/components/Typography/SubHeading';
import Paragraph from '@/components/Typography/Paragraph';
import FreeConsultationButton from '@/components/FreeConsultationButton';

// Style
import style from "./CourseEntryRequirements.style";

const  CourseEntryRequirements = () => {
  const courseEntryRequirementsData = [
    {
      title: 'Английский язык',
    },
    {
      title: 'Компьютерная грамотность',
    },
    {
      title: 'Мотивация',
    },
  ];

  return (
    <ScrollAnimation animateIn='fadeIn'>
      <section css={style()}>
        <SubHeading text="Требования к поступлению" type="h2" />
        <Paragraph
          text="В связи с тем, что данная программа направлена на качественное обучение и ваше трудоустройство мы вынуждены проводить предварительное собеседование и тестирование кандидатов по трем направлениям:"
          type="medium"
        />

        <ul className="course-entry-requirements-list">
          {courseEntryRequirementsData.map(item => (
            <li key={item.title}>
              <Paragraph text={item.title} type="medium" />
            </li>
          ))}
        </ul>

        <FreeConsultationButton />
      </section>
    </ScrollAnimation>
  );
};

export default  CourseEntryRequirements;