/** @jsxRuntime classic */
/** @jsx jsx */
import { css } from '@emotion/react';

const style = () => css`
  /* Common style */
  .SubHeading.h2 {
    margin-bottom: 24px;
  }

  .course-entry-requirements-list {
    list-style: disc;
    margin: 0 0 32px 16px;
  }
`;

export default style;