/** @jsxRuntime classic */
/** @jsx jsx */
import { useEffect, useState } from 'react';
import { jsx } from '@emotion/react';

// Components
import Header from './components/Header';
import MainAbout from './components/MainIntro';
import SwitchProfession from './components/SwitchProfession';
import FreeConsultation from './components/FreeConsultation';
import CoursePath from './components/CoursePath';
import DetailCourseProgram from './components/DetailCourseProgram';
import AfterCourse from './components/AfterCourse';
import CoursePriceInfo from './components/CoursePriceInfo';
import CourseEntryRequirements from './components/CourseEntryRequirements';
import CourseBenefits from './components/CourseBenefits';
import CourseTeachers from './components/CourseTeachers';
import AskedQuestion from './components/AskedQuestion';
import FeedbacksStudents from './components/FeedbacksStudents';
import CallButton from '@/components/CallButton';
import Contacts from '@/components/Contacts';
import Footer from '@/components/Footer';

// Hooks
import useDeviceDetect from '@/hooks/useDeviceDetect/index';

// Next components
import Bonus from './components/Bonus';
import LearnTechnology from './components/LearnTechnology';

// Style
import style from "./HomePage.style";

const HomePage = () => {
  // Constants
  const device = useDeviceDetect();

  return (
    <div css={style()}>
      <CallButton />
      <div className="container-fluid main-container-fluid">
        <div className="container">
          <Header />
          <MainAbout />
        </div>
      </div>

      <div className="container-fluid course-benefits-container-fluid">
        <div className="container container-mobile container-desktop">
          <CourseBenefits />
        </div>
      </div>

      <div className="container-fluid switch-profession-container-fluid">
        <div className="container container-mobile container-desktop">
          <SwitchProfession />
        </div>
      </div>

      <div className="container-fluid free-consultation-container-fluid">
        <div className="container container-mobile container-desktop">
          <FreeConsultation />
        </div>
      </div>

      <div className="container-fluid course-path-container-fluid">
        <div className="container container-mobile container-desktop">
          <CoursePath />
        </div>
      </div>

      <div className={`${device} container-fluid detail-course-program-container-fluid`}>
        <div className="container container-mobile container-desktop">
          <DetailCourseProgram />
        </div>
      </div>

      <div className="container-fluid bonus-container-fluid">
        <div className="container container-mobile container-desktop">
          <Bonus />
        </div>
      </div>

      <div className="container-fluid learn-technology-container-fluid">
        <div className="container container-mobile container-desktop">
          <LearnTechnology />
        </div>
      </div>

      <div className="container-fluid after-course-container-fluid">
        <div className="container container-mobile container-desktop">
          <AfterCourse />
        </div>
      </div>

      <div className="container-fluid course-price-container-fluid">
        <div className="container container-mobile container-desktop">
          <CoursePriceInfo />
        </div>
      </div>

      <div className="container-fluid course-entry-requirements-container-fluid">
        <div className="container container-mobile container-desktop">
          <CourseEntryRequirements />
        </div>
      </div>

      <div className="container-fluid course-teachers-container-fluid">
        <div className="container container-mobile container-desktop">
          <CourseTeachers />
        </div>
      </div>

      <div className="container-fluid asked-question-container-fluid">
        <div className="container container-mobile container-desktop">
          <AskedQuestion />
        </div>
      </div>

      <div className="container-fluid feedbacks-students-container-fluid">
        <div className="container container-mobile container-desktop">
          <FeedbacksStudents />
        </div>
      </div>

      <div className="container-fluid contacts-container-fluid">
        <div className="container container-mobile container-desktop">
          <Contacts />
        </div>
      </div>

      <div className="container-fluid">
        <div className="container container-mobile container-desktop">
          <Footer />
        </div>
      </div>
    </div>
  );
};

export default HomePage;