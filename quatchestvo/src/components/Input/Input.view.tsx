/** @jsxRuntime classic */
/** @jsx jsx */
import {
  useState,
  useEffect,
} from 'react';
import { jsx } from '@emotion/react';

// Hooks
import useDebounce from '@/hooks/useDebounce/index';

// Style
import style from "./Input.style";

interface IInput {
  disabled?: boolean,
  id: string,
  inputCustomClassName?: string,
  inputType: string,
  label?: string,
  placeholder: string,
  value: string | number,

  onChange: (args: { id: string, value: string | number }) => void,
}

const Input = (props: IInput) => {
  const {
    disabled = false,
    id,
    inputCustomClassName,
    inputType = 'text',
    label,
    placeholder,
    value,

    onChange,
  } = props;

  // State
  const [initComponent, setInitComponent] = useState(false);
  const [inputValue, setInputValue] = useState<string | number>(value);

  const debouncedInputValue = useDebounce(inputValue, 500);

  // Constants
  const inputClassName = inputCustomClassName ? `Input ${inputCustomClassName}` : 'Input';

  // Handlers
  const onFocusInputValueHandler = () => {
    setInitComponent(true);
  };

  const onChangeInputValueHandler = (event: React.ChangeEvent<HTMLInputElement>) => {
    setInputValue(event.target.value);
  };

  useEffect(() => {
    if (initComponent) {
      onChange({ id, value: debouncedInputValue });
    }
  }, [debouncedInputValue]);

  // Clear field value, after submit data from input
  useEffect(() => {
    if (value === '') {
      setInputValue('');
    }
  }, [value]);

  return (
    <div css={style()} >
      {label && (
        <label htmlFor={id}>{label}</label>
      )}
      <input
        className={inputClassName}
        id={id}
        type={inputType}
        placeholder={placeholder}
        value={inputValue}
        // maxLength={maxLength}
        onFocus={onFocusInputValueHandler}
        onChange={onChangeInputValueHandler}
        // onBlur={blurHandler}
        disabled={disabled}
      />
    </div>
  );
};

export default  Input;