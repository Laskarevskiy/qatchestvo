/** @jsxRuntime classic */
/** @jsx jsx */
import { css } from '@emotion/react';

// Colors
import { greyLight } from '@/common/color';


const style = () => css`
  /* Common style */
  display: flex;
  flex-direction: column;
  align-items: flex-start;

  font-size: 14px;
  line-height: 24px;

  label {
    font-weight: 500;
    color: #474A57;
    margin-bottom: 4px;
  }

  input {
    padding: 12px;
    background: ${greyLight};
    border-radius: 4px;
    border: none;
    outline: none;

    &::placeholder {
      color: #969BAB;
    }
  }
`;

export default style;