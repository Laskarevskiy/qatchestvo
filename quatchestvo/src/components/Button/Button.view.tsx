/** @jsxRuntime classic */
/** @jsx jsx */
import { jsx } from '@emotion/react';

// Style
import style from "./Button.style";

interface IButton {
  disabled?: boolean,
  heightButton?: '40' | '48',
  text: string,
  textSize: 'text-small' | 'text-medium',
  type: 'primary' | 'secondary' | 'white-button',
  onClick?: (...args: any) => void,
}

const Button = (props: IButton) => {
  const {
    disabled = false,
    heightButton,
    text,
    textSize,
    type,
    onClick,
  } = props;

  // Constants
  const heightButtonClassName = heightButton ? `size-${heightButton}` : 'size-48';
  const disabledClassName = disabled ? 'disabled-button' : '';
  const buttonClassName = `Button ${type} ${textSize} ${heightButtonClassName} ${disabledClassName}`;

  return (
    <button
      css={style()}
      className={buttonClassName}
      type="button"
      onClick={onClick}
      disabled={disabled}
    >
      {text}
    </button>
  );
};

export default Button;