/** @jsxRuntime classic */
/** @jsx jsx */
import { css } from '@emotion/react';

// Colors
import {
  primaryBlue,
  primaryBlueLight,
  primaryWhite,
  greyLight,
  neutralDarkGrey,
} from '@/common/color';

const style = () => css`
  &.Button {
    border-radius: 6px;
    border: none;
    cursor: pointer;

    /* Button type */
    &.primary {
      background: ${primaryBlue};
      color: ${primaryWhite};
    }

    &.secondary {
      background: ${primaryBlueLight};
      color: ${primaryBlue};
    }

    &.white-button {
      background: ${primaryWhite};
      color: ${primaryBlue};
    }

    /* Button text-size */
    &.text-small {
      font-size: 14px;
      line-height: 20px;
      font-weight: 500;
      text-align: center;
      padding: 10px 20px;
    }

    &.text-medium {
      font-size: 16px;
      font-weight: 500;
      line-height: 24px;
      text-align: center;
      padding: 12px 32px;
    }

    /* Button height */
    &.size-40 {
      min-height: 40px;
      height: 40px;
      line-height: normal;
    }

    &.size-48 {
      min-height: 48px;
    }

    /* Disabled button */
    &.disabled-button {
      background: ${greyLight};
      color: ${neutralDarkGrey};
      cursor: not-allowed;
    }
  }
`;

export default style;