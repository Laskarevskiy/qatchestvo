/** @jsxRuntime classic */
/** @jsx jsx */
import { jsx } from '@emotion/react';

// Components
import Caption from '@/components/Typography/Caption';

// Style
import style from "./Footer.style";

const  Footer = () => {

  return (
    <div css={style()}>
      <Caption text={`© ${new Date().getFullYear()} Qachestvo. All rights reserved`} type="medium" />
    </div>
  );
};

export default  Footer;