/** @jsxRuntime classic */
/** @jsx jsx */
import { css } from '@emotion/react';

// Config
import {
  mediaMobileTablet,
  mediaDesktop,
} from '@/config';

const style = () => css`
  /* Style mobile => tablet */
  ${mediaMobileTablet} {
    .Caption {
      text-align: center;
    }
  }

  /* Style desktop => */
  ${mediaDesktop} {
    .Caption {
      text-align: end;
    }
  }
`;

export default style;