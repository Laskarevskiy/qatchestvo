/** @jsxRuntime classic */
/** @jsx jsx */
import { css } from '@emotion/react';

// Colors
import { primaryWhite } from '@/common/color';

const style = () => css`
  @keyframes show {
    0% {
      display: none;
      opacity: 0;
    }
    1% {
      display: flex;
      opacity: 0;
    }
    100% {
      opacity: 1;
    }
  }

  /* Common style */
  &.modal-overlay {
    display: flex;
    align-items: center;
    justify-content: center;
    width: 100vw;
    height: 100vh;
    position: fixed;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    background: rgba(0, 19, 39, 0.4);
    animation: show .3s ease;
    overflow-y: scroll;
    z-index: 100;
  }

  .modal-container {
    position: relative;
    padding: 48px;
    border-radius: 12px;
    background: ${primaryWhite};
    min-height: 346px;
    max-width: 716px;
    width: 100%;
    margin: 0 16px;
  }

  .modal-close {
    display: inline-flex;
    position: absolute;
    top: 24px;
    right: 24px;
    border: none;
    background: transparent;
    padding: 0;
    cursor: pointer;
  }
`;

export default style;