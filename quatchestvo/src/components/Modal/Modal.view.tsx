/** @jsxRuntime classic */
/** @jsx jsx */
import ReactDOM from 'react-dom';
import {
  useCallback,
  useEffect,
  useRef,
} from 'react';
import { jsx } from '@emotion/react';

// Hooks
import useLockBodyScroll from '@/hooks/useLockBodyScroll/index';

// Icons
import IconCross from '@/static/images/IconCross.svg';

// Style
import style from "./Modal.style";

interface IModal {
  children: React.ReactNode,
  closeModalHandler: () => void,
}

const Modal = (props: IModal) => {
  const {
    children,
    closeModalHandler,
  } = props;

  // Ref
  const modal = useRef();
  const element: any = document.getElementById('modal');

  // Handlers
  const closeModalClickHandler = () => {
    closeModalHandler();
  };

  const handleKeyUp = useCallback((event: any) => {
    if (event.keyCode === 27) {
      event.preventDefault();
      closeModalHandler();
    }
  }, [closeModalHandler]);

  const handleOutsideClick = useCallback((event) => {
    if (modal !== null) {
      if (typeof event.target.className === 'string' && event.target.className.includes('modal-overlay')) {
        closeModalHandler();
      }
    }
  }, [modal, closeModalHandler]);

  useEffect(() => {
    window.addEventListener('keyup', handleKeyUp, false);
    window.addEventListener('click', handleOutsideClick, false);

    return () => {
      window.removeEventListener('keyup', handleKeyUp, false);
      window.removeEventListener('click', handleOutsideClick, false);
    };
  }, [handleKeyUp, handleOutsideClick]);

  useLockBodyScroll();
  return ReactDOM.createPortal(
    <div css={style()} className="modal-overlay">
      <div className="modal-container">
        <button
          type="button"
          className="modal-close"
          onClick={closeModalClickHandler}
        >
          <IconCross />
        </button>
        {children}
      </div>
    </div>,
    element,
  );
};

export default  Modal;