/** @jsxRuntime classic */
/** @jsx jsx */
import { css } from '@emotion/react';

// Config
import {
  mediaMobileTablet,
  mediaDesktop,
} from '@/config';

const style = () => css`
  /* Common style */

  .SubHeading.h4 {
    margin-bottom: 24px;
  }

  .Paragraph.small {
    margin-bottom: 32px;
  }

  /* Style mobile => tablet */
  ${mediaMobileTablet} {
  }

  /* Style desktop => */
  ${mediaDesktop} {
`;

export default style;