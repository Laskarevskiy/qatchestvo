/** @jsxRuntime classic */
/** @jsx jsx */
import { jsx } from '@emotion/react';

// Components
import SubHeading from '@/components/Typography/SubHeading';
import Paragraph from '@/components/Typography/Paragraph';
import LinkedinLink from '@/components/LinkedinLink';


// Style
import style from "./TeacherFullInformation.style";

interface ITeacherFullInformation {
  description: string,
  linkedinLink: string,
  linkedinName: string,
  name: string,
}

const  TeacherFullInformation = (props: ITeacherFullInformation) => {
  const {
    description,
    linkedinLink,
    linkedinName,
    name,
  } = props;


  return (
    <div css={style()}>
      <SubHeading text={name} type="h4"/>
      <Paragraph text={description} type="small" />
      <LinkedinLink
        link={linkedinLink}
        name={linkedinName}
        isLink
      />
    </div>
  );
};

export default  TeacherFullInformation;