/** @jsxRuntime classic */
/** @jsx jsx */
import React, { useState } from 'react';
import { jsx } from '@emotion/react';
import dynamic from 'next/dynamic';

// Components
import LinkedinLink from '@/components/LinkedinLink';
import TeacherFullInformation from './components/TeacherFullInformation';

// Dynamic Import Components
const Modal = dynamic(() => import('@/components/Modal'), { ssr: false });

// Style
import style from "./CardTeacher.style";

interface ICardTeacher {
  description: string,
  linkedinLink: string,
  linkedinName: string,
  name: string,
  photo: string,
  position: string,
}

const  CardTeacher = (props: ICardTeacher) => {
  const {
    description,
    linkedinLink,
    linkedinName,
    name,
    photo,
    position,
  } = props;

  // State
  const [showTeacherModal, setShowTeacherModal] = useState<boolean>(false);

  // Handlers
  const showInfoTeacherHandler = () => {
    setShowTeacherModal(true);
  };

  const closeModalHandler = () => {
    setShowTeacherModal(false);
  };

  return (
    <>
      <li css={style()} onClick={showInfoTeacherHandler}>
        <img
          className="teacher-photo"
          src={photo}
          alt={position}
        />
        <p className="teacher-name">{name}</p>
        <p className="teacher-position">{position}</p>

        <LinkedinLink name="Linkedin" />
      </li>

      {showTeacherModal && (
        <Modal closeModalHandler={closeModalHandler}>
          <TeacherFullInformation
            name={name}
            description={description}
            linkedinLink={linkedinLink}
            linkedinName={linkedinName}
          />
        </Modal>
      )}
    </>
  );
};

export default  CardTeacher;