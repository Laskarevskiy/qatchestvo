/** @jsxRuntime classic */
/** @jsx jsx */
import { css } from '@emotion/react';

// Colors
import { primaryGrey } from '@/common/color';

// Config
import { mediaMobileTablet } from '@/config';

const style = () => css`
  /* Common style */
  width: 255px;
  filter: grayscale(100%);
  transition: all 0.2s ease 0s;
  cursor: pointer;
  list-style: none;

  &:hover {
    filter: grayscale(0%);
  }

  .teacher-photo {
    width: 255px;
    height: 204px;
    object-fit: cover;
    border-radius: 4px;
    margin-bottom: 8px;
  }

  .teacher-name {
    font-size: 18px;
    line-height: 28px;
    font-weight: 500;
  }

  .teacher-position {
    font-size: 16px;
    line-height: 26px;
    color: ${primaryGrey};
    margin-bottom: 8px;
  }

  /* Style mobile => tablet */
  ${mediaMobileTablet} {
    filter: grayscale(0%);
  }
`;

export default style;