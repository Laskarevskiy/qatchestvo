/** @jsxRuntime classic */
/** @jsx jsx */
import { SyntheticEvent, useState } from 'react';
import { jsx } from '@emotion/react';
import axios from 'axios';
import { toast } from 'react-hot-toast';

// Components
import SubHeading from '@/components/Typography/SubHeading';
import Button from '@/components/Button';
import Input from '@/components/Input';

// Style
import style from "./CallBackForm.style";

interface ICallBackForm {
  analytics: string,
  closeCallbackFormModal?: (confirm: boolean) => void,
}

const  CallBackForm = (props: ICallBackForm)=> {
  const { analytics, closeCallbackFormModal } = props;

  // State
  const [callBackFormData, setCallBackFormData] = useState<{[key: string]: string | number}>({});

  // Constants
  const disabledButton = Object.values(callBackFormData).length >= 2 && !Object.values(callBackFormData).includes('');

  // Handlers
  const onChangeInputValueHandler = (args: { id: string, value: string | number }) => {
    const { id, value } = args;

    setCallBackFormData({
      ...callBackFormData,
      [id]: value,
    });
  };

  const submitBackCallHandler = async (event: SyntheticEvent) => {
    event.preventDefault();

    try {
      axios.post('https://v1.nocodeapi.com/laskarevskiy/google_sheets/yThlEIedDpACjNLL?tabId=Sheet_1', [
        [ callBackFormData.name, callBackFormData.phone, analytics, new Date().toLocaleString() ]
      ])
      toast.success('Successfully toasted!')

      if (closeCallbackFormModal) {
        closeCallbackFormModal(true);
      }
    } catch (error) {
      toast.error("Ой, что-то пошло не так. Попробуйте снова")
    }
    setCallBackFormData({});
  };

  return (
    <div css={style()}>
      <SubHeading text="Остались вопросы? " type="h4" />
      <SubHeading text="Мы с радостью ответим" type="h4" />
      <Input
        inputType="text"
        label="Введите имя"
        id="name"
        value={callBackFormData.name || ''}
        placeholder="Введите имя"
        onChange={onChangeInputValueHandler}
      />
      <Input
        inputType="number"
        label="Введите контактный номер"
        id="phone"
        value={callBackFormData.phone || ''}
        placeholder="Введите контактный номер"
        onChange={onChangeInputValueHandler}
      />
      <Button
        text="Перезвоните мне"
        textSize="text-small"
        type="primary"
        onClick={submitBackCallHandler}
        disabled={!disabledButton}
      />

    </div>
  );
};

export default  CallBackForm;