/** @jsxRuntime classic */
/** @jsx jsx */
import { css } from '@emotion/react';

// Config
import {
  mediaMobileTablet,
  mediaDesktop,
} from '@/config';

const style = () => css`
  /* Common style */

  Input {
    &:not(last-of-type) {
      margin-bottom: 24px;
    }
  }

  /* Style mobile => tablet */
  ${mediaMobileTablet} {
    .SubHeading:last-of-type {
      margin-bottom: 24px;
    }

    .Input {
      width: 100%;
    }

    .Button {
      width: 100%;
      margin-top: 24px;
    }
  }

  /* Style desktop => */
  ${mediaDesktop} {
    .SubHeading:last-of-type {
      margin-bottom: 32px;
    }

    .Input {
      max-width: 286px;
      width: 100%;
    }

    .Button {
      margin-top: 48px;
    }
  }
`;

export default style;