/** @jsxRuntime classic */
/** @jsx jsx */
import { css } from '@emotion/react';

// Config
import {
  mediaMobileTablet,
  mediaDesktop,
} from '@/config';

const style = () => css`
  @keyframes call-button-pulse {
    0% {
      transform: scale(0.95);
      box-shadow: 0 0 0 0 rgba(#FF9500, 0);
    }
    50% {
      transform: scale(1);
      box-shadow: 0 0 0 10px rgba(#FF9500, 0);

    }
    100% {
      transform: scale(0.95);
      box-shadow: 0 0 0 0 rgba(#FF9500, 0);
    }
  }

  button {
    position: fixed;
    bottom: 260px;
    display: flex;
    justify-content: center;
    align-items: center;
    border: none;
    background: #FF9500;
    padding: 24px;
    border-radius: 50%;
    width: 72px;
    height: 72px;
    box-shadow: rgb(0 0 0 / 24%) 2px 2px 24px;
    animation: call-button-pulse 1.5s infinite ease-in-out;
    cursor: pointer;
    z-index: 1;
    -webkit-tap-highlight-color: transparent;

    &:focus-visible {
      outline: none;
    }
  }

  /* Style mobile => tablet */
  ${mediaMobileTablet} {
    button {
      right: 20px;
      bottom: 20px;
    }
  }

  /* Style desktop => */
  ${mediaDesktop} {
    button {
      right: 78px;
    }
  }
`;

export default style;