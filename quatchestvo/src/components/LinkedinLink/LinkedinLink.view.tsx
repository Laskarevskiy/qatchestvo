/** @jsxRuntime classic */
/** @jsx jsx */
import { jsx } from '@emotion/react';

// Components

// Icons
import IconLinkedin from '@/static/images/IconLinkedin.svg';

// Style
import style from "./LinkedinLink.style";

interface ILinkedinLink {
  isLink?: boolean,
  link?: string,
  name?: string,
  textSize?: number,
}

const  LinkedinLink = (props: ILinkedinLink) => {
  const {
    isLink = false,
    link,
    name,
    textSize,
  } = props;

  // Constants
  const linkedinTextSize = textSize ? `size-${textSize}` : 'size-18';

  return (
    isLink ? (
      <a css={style()} href={link} target="_blank" rel="noreferrer">
        <IconLinkedin />
        <span className={`linkedin-link-name ${linkedinTextSize}`}>{name}</span>
      </a>
    ) : (
      <div css={style()}>
        <IconLinkedin />
        <span className={`linkedin-name ${linkedinTextSize}`}>{name}</span>
      </div>
    )
  );
};

export default  LinkedinLink;