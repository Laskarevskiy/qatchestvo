/** @jsxRuntime classic */
/** @jsx jsx */
import { css } from '@emotion/react';

// Colors
import { primaryBlue } from '@/common/color';

const style = () => css`
  /* Common style */
  display: flex;
  align-items: center;

  .IconLinkedin_svg__icon {
    min-width: 24px;
    min-height: 24px;
  }

  .linkedin-link-name {
    font-size: 14px;
    line-height: 24px;
    color: ${primaryBlue};
    margin-left: 8px;
  }

  .linkedin-name {
    font-size: 18px;
    line-height: 28px;
    font-weight: 500;
    margin-left: 8px;
  }

  /* Size */
  .size-18 {
    font-size: 18px;
  }
`;

export default style;