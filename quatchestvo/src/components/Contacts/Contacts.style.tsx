/** @jsxRuntime classic */
/** @jsx jsx */
import { css } from '@emotion/react';

// Config
import {
  mediaMobileTablet,
  mediaDesktop,
} from '@/config';

const style = () => css`
  /* Common style */
  .SubHeading.h2 {
    text-align: center;
  }

  .contact-wrap {
    display: flex;
    flex-direction: column;
    align-items: center;
    margin-bottom: 24px;

    .contact-link {
      margin-left: 8px;
    }
  }

  .contacts-social-links-wrap {
    display: flex;
    justify-content: center;

    .contact-social-link {
      cursor: pointer;

      &:not(:last-of-type) {
        margin-right: 16px;
      }
    }
  }

  /* Style mobile => tablet */
  ${mediaMobileTablet} {
    .SubHeading.h2 {
      margin-bottom: 24px;
    }

    .course-action-buttons {
      .Button {
        width: 100%;

        &:not(:last-of-type) {
          margin-bottom: 8px;
        }
      }
    }

    .contacts-social-links-wrap {
      margin-bottom: 24px;
    }
  }

  /* Style desktop => */
  ${mediaDesktop} {
    .SubHeading.h2 {
      margin-bottom: 48px;
    }

    .course-action-buttons {
      display: flex;
      justify-content: center;

      .Button:not(:last-of-type) {
        margin-right: 8px;
      }
    }

    .contacts-social-links-wrap {
      margin-bottom: 48px;
    }
  }
`;

export default style;