/** @jsxRuntime classic */
/** @jsx jsx */
import { jsx } from '@emotion/react';
import ScrollAnimation from 'react-animate-on-scroll';

// Components
import SubHeading from '@/components/Typography/SubHeading';
import Paragraph from '@/components/Typography/Paragraph';
import CourseActionButtons from '../CourseActionButtons';


// Icons
import IconFacebook from '@/static/images/IconFacebook.svg';
import IconTelegram from '@/static/images/IconTelegram.svg';

// Style
import style from "./Contacts.style";

const  Contacts = () => {

  return (
    <ScrollAnimation animateIn='fadeIn'>
      <section css={style()}>
        <SubHeading text="Наши контакты" type="h2" />

        <div className="contact-wrap">
          <Paragraph>
            Телефон:
            <a className="contact-link" href="tel:+380633117770">+380633117770</a>
          </Paragraph>
          <Paragraph>
            Skype:
            <a className="contact-link" href="skype:oleksii.asanov?call">oleksii.asanov</a>
          </Paragraph>

          <Paragraph text="Адрес: г. Киев Саксаганского 40" />
        </div>

        <div className="contacts-social-links-wrap">
          <a
            href="https://www.facebook.com/oleksii.asanov"
            className="contact-social-link"
            target="_blank"
            rel="noreferrer"
          >
            <IconFacebook />
          </a>
          <a href="https://t.me/oleksii_asanov" className="contact-social-link">
            <IconTelegram />
          </a>
        </div>

        <CourseActionButtons analytics="contacts" />
      </section>
    </ScrollAnimation>
  );
};

export default  Contacts;