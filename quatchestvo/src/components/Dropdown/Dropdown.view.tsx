/** @jsxRuntime classic */
/** @jsx jsx */
import { useState } from 'react';
import Link from 'next/link';
import { jsx } from '@emotion/react';
import { useRouter } from 'next/router';

// Components
import Caption from '@/components/Typography/Caption';

// Colors
import { primaryBlack } from '@/common/color';

// Style
import style from "./Dropdown.style";

const Dropdown = () => {
  const router = useRouter();

  // State
  const [showListLocales, setShowListLocales] = useState<boolean>(false);

  // Data
  const localesData = [
    {
      locale: "ua",
      text: "UA Українська",
      href: "/"
    },
    {
      locale: "ru",
      text: "RU Русский",
      href: "/"
    },
    {
      locale: "en",
      text: "EN English",
      href: "/"
    },
  ];

  // Constant
  const showListLocalesClassName = showListLocales ? 'show-list-locales' : '';

  // Handlers
  const showDropdownListClickHandler = () => setShowListLocales(prevState => !prevState);

  const selectCurrentLocaleClickHandler = () => setShowListLocales(prevState => !prevState);

  return (
    <div css={style()}>
      <div className="locale-container" onClick={showDropdownListClickHandler}>
        <Caption
          color={primaryBlack}
          customClassName={`current-locale ${showListLocalesClassName}`}
          type="medium"
          text={router.locale?.toString()}
        />
      </div>

      {showListLocales && (
        localesData.length > 0 && (
          <ul className="dropdown-list">
            {localesData.map(item => {
              const activeLocaleClassName = item.locale === router.locale ? 'active-locale' : '';

              return (
                <li
                  key={item.locale}
                  className={`dropdown-list__item ${activeLocaleClassName}`}
                  onClick={selectCurrentLocaleClickHandler}
                >
                  <Link href={item.href} locale={item.locale}>
                    <a>{item.text}</a>
                  </Link>
                </li>
              )
            })}
          </ul>
        )
      )}
    </div>
  )
}

export default Dropdown