/** @jsxRuntime classic */
/** @jsx jsx */
import { css } from '@emotion/react';

// Colors
import { primaryWhite, primaryBlue } from '@/common/color';

const style = () => css`
  position: relative;

  .locale-container {
    display: inline-block;
    cursor: pointer;

    .current-locale {
      display: flex;
      align-items: center;
      font-size: 14px;
      font-weight: 500;
      line-height: 20px;
      text-transform: uppercase;

      &::after {
        content: '';
        display: inline-block;
        background-image: url('/static/images/Arrow.svg');
        width: 18px;
        min-width: 18px;
        height: 18px;
        min-height: 18px;
        background-position: center;
        background-repeat: no-repeat;
        margin-left: 8px;
        transition: all 0.2s ease 0s;
      }

      &.show-list-locales {
        &::after {
          content: '';
          transform: rotate(180deg);
        }
      }
    }
  }

  .dropdown-list {
    position: absolute;
    top: 44px;
    right: 2px;
    width: 172px;
    background: ${primaryWhite};
    box-shadow: 0px 30px 40px rgba(212, 217, 232, 0.2);
    border-radius: 12px;
    padding: 24px;
    z-index: 1000;

    &::after {
      content: '';
      display: inline-block;
      position: absolute;
      top: -10px;
      right: 20px;
      transform: rotate(45deg);
      width: 24px;
      height: 24px;
      background: ${primaryWhite};
      border-radius: 3px;
    }

    &__item {
      display: block;
      font-size: 14px;
      line-height: 24px;
      text-align: center;
      cursor: pointer;

      &:not(:last-of-type) {
        margin-bottom: 16px;
      }

      &.active-locale {
        color: ${primaryBlue} !important;
      }
    }
  }
`;

export default style;