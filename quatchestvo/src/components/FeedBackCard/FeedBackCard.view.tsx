/** @jsxRuntime classic */
/** @jsx jsx */
import {
  useRef,
  useEffect,
  useState,
} from 'react';
import { jsx } from '@emotion/react';
import SmoothCollapse from 'react-smooth-collapse';

// Components
import SubHeading from '@/components/Typography/SubHeading';
import Caption from '@/components/Typography/Caption';
import LinkedinLink from '@/components/LinkedinLink';

// Hooks
import useDeviceDetect from '@/hooks/useDeviceDetect/index';

// Icons
import IconQuotes from '@/static/images/IconQuotes.svg';

// Style
import style from "./FeedBackCard.style";

interface IFeedBackCard {
  feedbackDescription: string,
  userName: string,
  userPosition: string,
  userLinkedinLink: string,
}

const  FeedBackCard = (props: IFeedBackCard) => {
  const {
    feedbackDescription,
    userName,
    userPosition,
    userLinkedinLink,
  } = props;

  // State
  const [isVisibleButton, setIsVisibleButton] = useState<boolean>(false);
  const [isShowFullDescription, setShowFullDescription] = useState(false);

  // Refs
  const descriptionRef = useRef<any>(null);

  // Constants
  const device = useDeviceDetect();
  const lineDescription = 208; // 8 lines

  // Handlers
  const toggleDescriptionClickHandler = () => {
    setShowFullDescription(prevState => !prevState);
  };

  useEffect(() => {
    if(typeof window !== 'undefined') {
      const isVisibleButton = descriptionRef?.current?.scrollHeight > lineDescription;

      setIsVisibleButton(isVisibleButton);
    }
  }, [descriptionRef]);

  return (
    <li css={style()} className="feedbacks-card">
      <IconQuotes />

      {device !== 'desktop' ? (
        <SmoothCollapse
        expanded={isShowFullDescription}
        heightTransition=".35s ease"
        collapsedHeight={isVisibleButton ? `${lineDescription}px` : '100%'}
        >
          <p ref={descriptionRef} className="feedback-description">
            {feedbackDescription}
          </p>
        </SmoothCollapse>
      ) : (
        <p className="feedback-description">
          {feedbackDescription}
        </p>
      )}

      {isVisibleButton && (
        <button
          type="button"
          className="description-toggle-button"
          onClick={toggleDescriptionClickHandler}
        >
          {isShowFullDescription ? 'Скрыть' : 'Смотреть больше'}
        </button>
      )}

      <div className="user-info-container">
        <SubHeading text={userName} type="h6" />
        <Caption text={userPosition} type="medium" />
      </div>

      <LinkedinLink isLink link={userLinkedinLink} name="Linkedin" />
    </li>
  );
};

export default  FeedBackCard;