/** @jsxRuntime classic */
/** @jsx jsx */
import { css } from '@emotion/react';

// Colors
import { primaryWhite, primaryBlue } from '@/common/color';

// Config
import {
  mediaMobileTablet,
  mediaDesktop,
} from '@/config';

const style = () => css`
  /* Common style */
  position: relative;
  max-width: 445px;
  background: ${primaryWhite};
  box-shadow: 0px 10px 20px rgba(41, 41, 42, 0.07);
  border-radius: 8px;

  .user-info-container {
    display: flex;
    flex-direction: column;
    align-items: flex-start;
    margin-bottom: 16px;

    .SubHeading {
      font-weight: 700;
    }
  }

  /* Style mobile => tablet */
  ${mediaMobileTablet} {
    padding: 46px 16px 16px;

    .IconQuotes_svg__icon {
      position: absolute;
      top: 16px;
      left: 16px;
    }

    .feedback-description {
      font-size: 16px;
      line-height: 26px;
      margin-bottom: 16px;
    }

    .description-toggle-button {
      font-size: 16px;
      line-height: 26px;
      font-weight: 600;
      color: ${primaryBlue};
      border: none;
      background: transparent;
      padding: 0;
      margin: 4px 0 16px;
      cursor: pointer;
    }
  }

  /* Style desktop => */
  ${mediaDesktop} {
    padding: 32px 32px 32px 66px;

    .IconQuotes_svg__icon {
      position: absolute;
      top: 32px;
      left: 32px;
    }

    .feedback-description {
      font-size: 18px;
      line-height: 32px;
      margin-bottom: 24px;
    }

    .description-toggle-button {
      display: none;
    }
  }
`;

export default style;