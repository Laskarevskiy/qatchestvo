/** @jsxRuntime classic */
/** @jsx jsx */
import { useState } from 'react';
import { jsx } from '@emotion/react';
import dynamic from 'next/dynamic';

// Components
import Button from '@/components/Button';
import CallBackForm from '@/components/CallBackForm';

// Style
import style from './FreeConsultationButton.style';

// Dynamic Import Components
const Modal = dynamic(() => import('@/components/Modal'), { ssr: false });

const FreeConsultationButton = () => {
  // State
  const [showModalCallBack, setShowModalCallBack] = useState(false);

  // Handlers
  const callButtonClickHandler = () => {
    setShowModalCallBack(prevState => !prevState);
  };

  return (
    <div css={style()}>
      <Button
        text="Получить Консультацию"
        textSize="text-small"
        type="primary"
        onClick={callButtonClickHandler}
      />

      {showModalCallBack && (
        <Modal closeModalHandler={callButtonClickHandler}>
          <CallBackForm closeCallbackFormModal={callButtonClickHandler} analytics="free_consultation" />
        </Modal>
      )}
    </div>
  );
};

export default FreeConsultationButton;
