/** @jsxRuntime classic */
/** @jsx jsx */
import { css } from '@emotion/react';

// Config
import { mediaMobileTablet } from '@/config';

const style = () => css`
  /* Style mobile => tablet */
  ${mediaMobileTablet} {
    .Button {
      width: 100%;
    }
  }
`;

export default style;