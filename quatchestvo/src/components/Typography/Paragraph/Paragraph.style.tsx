/** @jsxRuntime classic */
/** @jsx jsx */
import { css } from '@emotion/react';

// Config
import {
  mediaMobileTablet,
  mediaDesktop,
} from '@/config';

const style = () => css`
  &.Paragraph {
    &.weight-400 {
      font-weight: 400;
    }

    &.weight-500 {
      font-weight: 500;
    }

    &.weight-600 {
      font-weight: 600;
    }

    &.weight-700 {
      font-weight: 700;
    }
  }

  /* Style mobile => tablet */
  ${mediaMobileTablet} {
    &.Paragraph {

      &.small {
        font-size: 14px;
        line-height: 24px;
      }

      &.medium {
        font-size: 16px;
        line-height: 26px;
      }
    }
  }

  /* Style desktop => */
  ${mediaDesktop} {
    &.Paragraph {

      &.small {
        font-size: 16px;
        line-height: 26px;
      }

      &.medium {
        font-size: 16px;
        line-height: 26px;
      }
    }
  }
`;

export default style;