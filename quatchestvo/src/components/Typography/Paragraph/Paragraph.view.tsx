/** @jsxRuntime classic */
/** @jsx jsx */
import { jsx } from '@emotion/react';

// Style
import style from "./Paragraph.style";

interface IParagraph {
  text?: string,
  type?: 'small' | 'medium',
  weight?: '400' | '500' | '600' | '700',
  children?: React.ReactNode,
  customClassName?: string,
}

const Paragraph = (props: IParagraph) => {
  const {
    text,
    type,
    weight,
    children,
    customClassName,
  } = props;

  // Constants
  const paragraphTypeClassName = type || 'medium';
  const weightClassName = weight ? `weight-${weight}` : 'weight-400';
  const paragraphClassName = `Paragraph ${weightClassName} ${paragraphTypeClassName} ${customClassName || ''}`;

  return (
    <p css={style} className={paragraphClassName}>
      {text || children}
    </p>
  );
};

export default Paragraph;