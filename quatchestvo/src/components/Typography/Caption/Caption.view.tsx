/** @jsxRuntime classic */
/** @jsx jsx */
import { jsx } from '@emotion/react';
import React from 'react';

// Colors
import { primaryGrey } from '@/common/color';

// Style
import style from "./Caption.style";

interface ICaption {
  children?: React.ReactNode,
  color?: string,
  customClassName?: string,
  text?: string,
  type: 'small' | 'medium',
  onClick?: (...args: any) => void,
}

const Caption = (props: ICaption) => {
  const {
    children,
    color,
    customClassName,
    text,
    type,
    onClick,
  } = props;

  // Constants
  const customColor = color || primaryGrey;
  const className = customClassName || '';
  const captionClassName = `Caption ${type} ${className}`;

  return (
    <span
      css={style}
      className={captionClassName}
      style={{ color: customColor }}
      onClick={onClick}
    >
      {text || children}
    </span>
  );
};

export default Caption;