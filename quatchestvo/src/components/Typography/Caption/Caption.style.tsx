/** @jsxRuntime classic */
/** @jsx jsx */
import { css } from '@emotion/react';

const style = () => css`
  &.Caption {
    display: block;
    text-align: center;

    &.small {
      font-size: 12px;
      font-weight: 500;
      line-height: 16px;
    }

    &.medium {
      font-size: 14px;
      line-height: 24px;
    }
`;

export default style;