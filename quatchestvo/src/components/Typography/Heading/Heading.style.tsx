/** @jsxRuntime classic */
/** @jsx jsx */
import { css } from '@emotion/react';

const style = () => css`
  &.Heading {
    font-size: 48px;
    line-height: 64px;
    font-weight: 800;
  }
`;

export default style;