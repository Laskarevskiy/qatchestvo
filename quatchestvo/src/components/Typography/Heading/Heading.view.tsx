/** @jsxRuntime classic */
/** @jsx jsx */
import { jsx } from '@emotion/react';

// Style
import style from "./Heading.style";

interface IHeading {
  text: string,
}

const Heading = (props: IHeading) => {
  const { text } = props;

  // Constants
  const headingClassName = `Heading`;

  return (
    <h1 css={style} className={headingClassName}>
      {text}
    </h1>
  );
};

export default Heading;