/** @jsxRuntime classic */
/** @jsx jsx */
import { css } from '@emotion/react';

// Config
import {
  mediaMobileTablet,
  mediaDesktop,
} from '@/config';

const style = () => css`
  /* Style mobile => tablet */
  ${mediaMobileTablet} {
    &.SubHeading {
      &.h2 {
        font-size: 28px;
        font-weight: 700;
        line-height: 40px;
      }

      &.h4 {
        font-size: 24px;
        font-weight: 600;
        line-height: 32px;
      }

      &.h5 {
        font-size: 20px;
        font-weight: 500;
        line-height: 30px;
      }

      &.h6 {
        font-size: 18px;
        font-weight: 500;
        line-height: 28px;
      }
    }
  }

  /* Style desktop => */
  ${mediaDesktop} {
    &.SubHeading {
      &.h2 {
        font-size: 40px;
        font-weight: 800;
        line-height: 54px;
      }

      &.h4 {
        font-size: 28px;
        font-weight: 700;
        line-height: 40px;
      }

      &.h5 {
        font-size: 24px;
        font-weight: 600;
        line-height: 32px;
      }

      &.h6 {
        font-size: 18px;
        font-weight: 500;
        line-height: 28px;
      }
    }
  }
`;

export default style;