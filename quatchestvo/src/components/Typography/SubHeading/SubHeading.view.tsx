/** @jsxRuntime classic */
/** @jsx jsx */
import { jsx } from '@emotion/react';

// Style
import style from "./SubHeading.style";

interface ISubHeading {
  text?: string,
  type: 'h2' | 'h4' | 'h5' | 'h6',
  children?: React.ReactNode,
}

const  SubHeading = (props: ISubHeading) => {
  const {
    text,
    type,
    children,
  } = props;

  // Constants
  const reactNode = text || children;
  const subHeadingClassName = `SubHeading ${type}`;

    switch (type) {
      case 'h2':
        return (
          <h2 css={style()} className={subHeadingClassName}>
            {reactNode}
          </h2>
        )

      case 'h4':
        return (
          <h4 css={style()} className={subHeadingClassName}>
            {reactNode}
          </h4>
        )

      case 'h5':
        return (
          <h5 css={style()} className={subHeadingClassName}>
            {reactNode}
          </h5>
        )

      case 'h6':
        return (
          <h6 css={style()} className={subHeadingClassName}>
            {reactNode}
          </h6>
        )

      default:
        return null;
    }
};

export default  SubHeading;