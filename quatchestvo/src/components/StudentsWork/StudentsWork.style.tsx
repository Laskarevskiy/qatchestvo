/** @jsxRuntime classic */
/** @jsx jsx */
import { css } from '@emotion/react';

// Colors
import { primaryBlue } from '@/common/color';

const style = () => css`
  .Caption {
    text-align: start;

    .main-intro__link-description {
      text-decoration: underline;
      color: ${primaryBlue};
    }
  }
`;

export default style;