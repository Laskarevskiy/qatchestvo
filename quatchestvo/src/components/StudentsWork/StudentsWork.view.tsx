/** @jsxRuntime classic */
/** @jsx jsx */
import { jsx } from '@emotion/react';

// Components
import Caption from '@/components/Typography/Caption';

// Style
import style from './StudentsWork.style';

const StudentsWork = () => {

  return (
    <div css={style()} className="student-work">
      <Caption type="small">
        <a href="#" className="main-intro__link-description">Узнайте у бывших студентов, где они работают </a>
        и как проходило обучение сами. Они не против
      </Caption>
    </div>
  );
};

export default StudentsWork;