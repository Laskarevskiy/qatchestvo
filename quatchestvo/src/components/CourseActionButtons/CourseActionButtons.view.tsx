import React from 'react';
import { useState } from 'react';
import dynamic from 'next/dynamic';

// Translation
import { injectIntl } from 'react-intl';

// Components
import Button from '@/components/Button';
import CallBackForm from '@/components/CallBackForm';

// Helpers
import { getTranslations } from '@/helpers/getTranslations';

// Dynamic Import Components
const Modal = dynamic(() => import('@/components/Modal'), { ssr: false });
interface ICourseActionButtons {
  analytics: string,
  heightButton?: '40' | '48',
  intl: any,
}

const CourseActionButtons = (props: ICourseActionButtons) => {
  const {
    analytics,
    heightButton,
    intl,
  } = props;

  // State
  const [showModalCallBack, setShowModalCallBack] = useState(false);

  // Translation
  const translation = getTranslations(intl);

  // Handlers
  const showModalCallBackClickHandler = () => {
    setShowModalCallBack(prevState => !prevState);
  };

  return (
    <>
    <div className="course-action-buttons">
      <Button
        heightButton={heightButton}
        text={translation.TRIAL_LESSON}
        textSize="text-medium"
        type="secondary"
        onClick={showModalCallBackClickHandler}
      />
      <Button
        heightButton={heightButton}
        text={translation.SING_TRAINING}
        textSize="text-medium"
        type="primary"
        onClick={showModalCallBackClickHandler}
      />
    </div>

    {showModalCallBack && (
        <Modal closeModalHandler={showModalCallBackClickHandler}>
          <CallBackForm closeCallbackFormModal={showModalCallBackClickHandler} analytics={analytics} />
        </Modal>
      )}
    </>
  );
};

export default injectIntl(CourseActionButtons);
