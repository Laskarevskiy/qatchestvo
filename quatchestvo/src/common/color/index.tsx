export const primaryBlue = '#007AFF';
export const primaryBlueLight = 'rgba(0, 122, 255, 0.15)';

export const primaryWhite = '#FFFFFF';
export const primaryBlack = '#18191F';

export const primaryGrey = '#A1A8BD';
export const secondaryGrey = '#F4F5F7';
export const greyLight = '#EEEFF4';
export const neutralDarkGrey = '#737D9B';
