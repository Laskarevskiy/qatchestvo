/** @jsxRuntime classic */
/** @jsx jsx */
import { css } from '@emotion/react';

// Colors
import { primaryBlack } from '../color';

const style = () => css`
  html,
  body {
    padding: 0;
    margin: 0;
    font-family: Inter, -apple-system, BlinkMacSystemFont, Segoe UI, Roboto, Oxygen,
      Ubuntu, Cantarell, Fira Sans, Droid Sans, Helvetica Neue, sans-serif;
    color: ${primaryBlack};
  }

  a {
    color: inherit;
    text-decoration: none;
  }

  * {
    box-sizing: border-box;
  }

  .container-fluid {
    width: 100%;
  }

  .container {
    display: block;
    max-width: 1110px;
    width: 100%;
    margin: 0 auto;
    padding: 0 16px;
  }
`;

export default style;