export const getTranslations = (intl: any) => {
  return {
    ...intl.messages,
  }
}