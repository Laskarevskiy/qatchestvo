export const mediaMobileTablet = '@media (min-width: 320px) and (max-width: 767px)';
export const mediaMobileMax = '@media (max-width: 425px)';
export const mediaDesktop = '@media (min-width: 768px)';

export const DEVICE_DESKTOP = 'desktop';
export const DEVICE_MOBILE = 'mobile';

